#include<list>
#include<iostream>
#include"BoolMap.hpp"

const int ORIGIN = -1;

class PathFinder
{
    public:
        static std::string findPath(int startIndex, int destIndex, const BoolMap* boolMap, int objWidth, int objHeight);

    private:
        class PathNode
        {
            public:
                PathNode(): parent(NULL){}
                PathNode(int g,int h,int ind,PathNode* par): G(g), H(h), F(g + h), index(ind), parent(par){}
                int G;
                int H;
                int F;
                int index;
                PathNode *parent;
        };

        static bool checkMap(int index,int width,int height,const BoolMap *map);
        static bool checkDest(int start,int dest,int width,int height,int mapwidth);
        static int findDistance(int,int,int);
        static std::string to_string(int);
        static std::string tracePath(PathNode*);
        static std::list<PathNode> openList;
        static std::list<PathNode> closedList;
};
