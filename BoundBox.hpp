#ifndef BOX
#define BOX

#include<iostream>
#include<list>
#include<vector>

using namespace std;

class Box
{
    public:
        Box(short int X,
            short int Y,
            short int W,
            short int H):x(X),y(Y),w(W),h(H){}

        short int getX(){return x;}
        short int getY(){return y;}
        short int getW(){return w;}
        short int getH(){return h;}

    private:
        short int x;
        short int y;
        short int w;
        short int h;
};

class CollisionBox
{
    public:
        CollisionBox(short int w,short int h) : width(w),height(h){}

        void addStateBox(short int,
                         short int,
                         short int,
                         short int,
                         short int);

        void deleteBoxes();
        bool testBox(Box *);

    private:
        vector<list<Box*> > StateBoxes;
        short int width;
        short int height;
};
#endif
