#ifndef EVENTLISTENER
#define EVENTLISTENER

#include"Event.hpp"

class Event;

/*
*Base class for all EventListeners
*/
class EventListener
{
    public:
        virtual ~EventListener(){}
        virtual void onEvent(const EVENT_TYPE &,const EventUnion &,const FilterUnion &){}
};
#endif
