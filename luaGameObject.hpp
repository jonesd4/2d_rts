#ifndef LUAGAMEOBJECT
#define LUAGAMEOBJECT

#include"luawrapper.hpp"
#include"tile.hpp"

GameObject *GameObject_new(lua_State *L);

int GameObject_getType(lua_State *L);
int GameObject_getImgRef(lua_State *L);
int GameObject_getImgRect(lua_State *L);
int GameObject_getAbsX(lua_State *L);
int GameObject_getAbsY(lua_State *L);
int GameObject_getX(lua_State *L);
int GameObject_getY(lua_State *L);
int GameObject_getFaction(lua_State *L);
int GameObject_getWidth(lua_State *L);
int GameObject_getHeight(lua_State *L);

int GameObject_setObjType(lua_State *L);
int GameObject_setImgRef(lua_State *L);
int GameObject_setImgRect(lua_State *L);
int GameObject_setAbsX(lua_State *L);
int GameObject_setAbsY(lua_State *L);
int GameObject_setX(lua_State *L);
int GameObject_setY(lua_State *L);
int GameObject_setFaction(lua_State *L);
int GameObject_setWidth(lua_State *L);
int GameObject_setHeight(lua_State *L);

int GameObject_addMoveCom(lua_State *L);
int GameObject_addAttackableCom(lua_State *L);
int GameObject_addAttackerCom(lua_State *L);
int GameObject_addResourceCom(lua_State *L);
int GameObject_addBuilderCom(lua_State *L);
int GameObject_addBuildingCom(lua_State *L);
int GameObject_addAnimationCom(lua_State *L);


int luaopen_GameObject(lua_State *L);

static luaL_Reg GameObject_table[] = {
    {NULL,NULL}
};

static luaL_Reg GameObject_metatable[] = {
    {"getType",GameObject_getType},
    {"getImgRef",GameObject_getImgRef},
    {"getImgRect",GameObject_getImgRect},
    {"getAbsX",GameObject_getAbsX},
    {"getAbsY",GameObject_getAbsY},
    {"getX",GameObject_getX},
    {"getY",GameObject_getY},
    {"getFaction",GameObject_getFaction},
    {"getWidth",GameObject_getWidth},
    {"getHeight",GameObject_getHeight},
    {"setObjType",GameObject_setObjType},
    {"setImgRef",GameObject_setImgRef},
    {"setImgRect",GameObject_setImgRect},
    {"setAbsX",GameObject_setAbsX},
    {"setAbsY",GameObject_setAbsY},
    {"setX",GameObject_setX},
    {"setY",GameObject_setY},
    {"setFaction",GameObject_setFaction},
    {"setWidth",GameObject_setWidth},
    {"setHeight",GameObject_setHeight},
    {"addMoveCom",GameObject_addMoveCom},
    {"addAttackableCom",GameObject_addAttackableCom},
    {"addAttackerCom",GameObject_addAttackerCom},
    {"addBuilderCom",GameObject_addBuilderCom},
    {"addBuildingCom",GameObject_addBuildingCom},
    {"addResourceCom",GameObject_addResourceCom},
    {"addAnimationCom",GameObject_addAnimationCom},
    { NULL,NULL }
};

#endif
