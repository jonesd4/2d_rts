#ifndef GAMESTRUCTS
#define GAMESTRUCTS

class GameObject;

struct Vector2
{
    int x;
    int y;
};

struct GameObjVec
{
    GameObject *gobjOne;
    GameObject *gobjTwo;
};

class Square
{
    public:
        Square(int X,int Y,int W,int H):x(X), y(Y), width(W), height(H),left(X),right(X + W), top(Y), bottom(Y + H)
        {}

        int getX(){return x;}
        int getY(){return y;}
        int getWidth(){return width;}
        int getHeight(){return height;}
        int getLeft(){return left;}
        int getRight(){return right;}
        int getTop(){return top;}
        int getBottom(){return bottom;}

        bool overlap(Square &);

    private:
        int x;
        int y;
        int width;
        int height;
        int left;
        int right;
        int top;
        int bottom;
};
#endif
