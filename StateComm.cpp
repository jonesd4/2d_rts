#include"StateComm.hpp"

StateComm::StateComm(): state(LOAD), dead(false)
{}

void StateComm::run()
{
    switch(state)
    {
        case LOAD:
            onLoad();
            break;

        case LOOP:
            onLoop();
            break;

        case CLOSE:
            onClose();
            break;
    }
}
