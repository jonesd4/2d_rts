#ifndef LUAPLAYERCONTROLLER
#define LUAPLAYERCONTROLLER

#include"luawrapper.hpp"
#include"GameStates.hpp"

class PlayerController;

PlayerController *PlayerController_new(lua_State *L);

int PlayerController_getFaction(lua_State *L);

int luaopen_PlayerController(lua_State *L);

static luaL_Reg PlayerController_table[] = {
    {NULL,NULL}
};

static luaL_Reg PlayerController_metatable[] = {
    {"getFaction",PlayerController_getFaction},
    { NULL,NULL }
};
#endif
