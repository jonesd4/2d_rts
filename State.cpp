
#include "State.hpp"

State::State()
{
    state = LOAD; //Set default state to load.
}

/*Call method based on state.*/
void State::run()
{
    switch(state)
    {
        case LOAD:
            onLoad();
            break;

        case LOOP:
            onLoop();
            break;

        case CLOSE:
            onClose();
            break;
    }
}
