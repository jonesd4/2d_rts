#include"luaGameObject.hpp"
#include"GameCom.hpp"

GameObject *GameObject_new(lua_State *L)
{
    /*DO NOT USE THIS YET!!! Need to do lua wrapper for MapState first.*/
    GameObject DO_NOT_USE = GameObject(SOLID,0,0,0,0,0,0,0,0,0);
    return &DO_NOT_USE;
}

int GameObject_getType(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    lua_pushnumber(L,gobj->getType());
    return 1;
}

int GameObject_getImgRef(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    lua_pushnumber(L,gobj->getImgRef());
    return 1;
}

int GameObject_getImgRect(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    lua_pushnumber(L,gobj->getImgRect());
    return 1;
}

int GameObject_getAbsX(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    lua_pushnumber(L,gobj->getAbsX());
    return 1;
}

int GameObject_getAbsY(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    lua_pushnumber(L,gobj->getAbsY());
    return 1;
}

int GameObject_getX(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    lua_pushnumber(L,gobj->getX());
    return 1;
}

int GameObject_getY(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    lua_pushnumber(L,gobj->getY());
    return 1;
}

int GameObject_getFaction(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    lua_pushnumber(L,gobj->getFaction());
    return 1;
}

int GameObject_getWidth(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    lua_pushnumber(L,gobj->getWidth());
    return 1;
}

int GameObject_getHeight(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    lua_pushnumber(L,gobj->getHeight());
    return 1;
}

int GameObject_setImgRef(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    gobj->setImgRef(luaL_checknumber(L,2));
    return 0;
}

int GameObject_setImgRect(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    gobj->setImgRect(luaL_checknumber(L,2));
    return 0;
}

int GameObject_setAbsX(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    gobj->setAbsX(luaL_checknumber(L,2));
    return 0;
}

int GameObject_setAbsY(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    gobj->setAbsY(luaL_checknumber(L,2));
    return 0;
}

int GameObject_setX(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    gobj->setX(luaL_checknumber(L,2));
    return 0;
}

int GameObject_setY(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    gobj->setY(luaL_checknumber(L,2));
    return 0;
}

int GameObject_setFaction(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    gobj->setFaction(luaL_checknumber(L,2));
    return 0;
}

int GameObject_setWidth(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    gobj->setWidth(luaL_checknumber(L,2));
    return 0;
}

int GameObject_setHeight(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    gobj->setHeight(luaL_checknumber(L,2));
    return 0;
}

int GameObject_setObjType(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    gobj->setObjType(luaL_checknumber(L,2));
    return 0;
}

int GameObject_addMoveCom(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    gobj->addCom(luaW_check<moveCom>(L,-1));
    return 0;
}

int GameObject_addAttackableCom(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    gobj->addCom(luaW_check<attackableCom>(L,-1));
    return 0;
}

int GameObject_addAttackerCom(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    gobj->addCom(luaW_check<attackerCom>(L,-1));
    return 0;
}

int GameObject_addResourceCom(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    gobj->addCom(luaW_check<resourceCom>(L,-1));
    return 0;
}

int GameObject_addBuilderCom(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    gobj->addCom(luaW_check<builderCom>(L,-1));
    return 0;
}

int GameObject_addBuildingCom(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    gobj->addCom(luaW_check<buildingCom>(L,-1));
    return 0;
}

int GameObject_addAnimationCom(lua_State *L)
{
    GameObject *gobj = luaW_check<GameObject>(L,1);
    gobj->addCom(luaW_check<animationCom>(L,-1));
    return 0;
}

int luaopen_GameObject(lua_State *L)
{
    luaW_register<GameObject>(L, "GameObject", GameObject_table, GameObject_metatable, GameObject_new);
    return 1;
}
