#include<sstream>
#include<string>
#include<iostream>

#include"PathFinder.hpp"
#include"utilities.hpp"

std::list<PathFinder::PathNode> PathFinder::openList;
std::list<PathFinder::PathNode> PathFinder::closedList;

std::string PathFinder::to_string(int number){
    std::string number_string = "";
    char ones_char;
    int ones = 0;
    while(true){
        ones = number % 10;
        switch(ones){
            case 0: ones_char = '0'; break;
            case 1: ones_char = '1'; break;
            case 2: ones_char = '2'; break;
            case 3: ones_char = '3'; break;
            case 4: ones_char = '4'; break;
            case 5: ones_char = '5'; break;
            case 6: ones_char = '6'; break;
            case 7: ones_char = '7'; break;
            case 8: ones_char = '8'; break;
            case 9: ones_char = '9'; break;
        }
        number -= ones;
        number_string = ones_char + number_string;
        if(number == 0){
            break;
        }
        number = number/10;
    }
    return number_string;
}

bool PathFinder::checkMap(int index,int width,int height,const BoolMap *map)
{
    bool flag = false;
    int x = index % map->getWidth();
    int y = index / map->getWidth();

    for(int i = 0; i < height && !flag; i++)
    {
        for(int j = 0; j < width && !flag; j++)
        {
            if(map->get((x + j),(y + i)))
                flag = true;
        }
    }

    return flag;
}

bool PathFinder::checkDest(int start, int dest, int width, int height, int mapwidth)
{
    for(int i = 0; i < height; i++)
    {
        for(int j = 0; j < width; j++)
        {
            if((start + (j * 1) + (i * mapwidth)) == dest)
                return true;
        }
    }

    return false;
}

int PathFinder::findDistance(int a, int b, int width)
{
        int aMod = a % width;
        int bMod = b % width;
        int aDiv = a / width;
        int bDiv = b / width;

        int xDiff = (aMod > bMod)?(aMod - bMod):(bMod - aMod);
        int yDiff = (aDiv > bDiv)?(aDiv - bDiv):(bDiv - aDiv);

        return (xDiff + yDiff);
}

std::string PathFinder::tracePath(PathNode *node)
{
    std::string path = "";

    while(node->parent != NULL)
    {
        //ss << node->index;
        path += strFromInt(node->index) + "\n";
        node = node->parent;
    }

    if(path == "")
        path = "-1";
    else
        path += "-1";

    return path;
}

std::string PathFinder::findPath(int startIndex, int destIndex, const BoolMap *boolMap, int objwidth, int objheight)
{
    /*Clear the lists to start new find.*/
    openList.clear();
    closedList.clear();

    /*Get the values needed from current map.*/
    int width = boolMap->getWidth();
    int height = boolMap->getHeight();

    /*Set origin of path*/
    PathNode start;
    start.index = startIndex;
    start.G = 0;

    /*Find simple distance between origin and destination.*/
    start.H = findDistance(startIndex,destIndex,width);
    start.F = start.G + start.H;
    start.parent = NULL;

    openList.push_front(start); //Start node is the first node to be searched.

    while(!openList.empty())
    {
        PathNode current;
        int min = 1000; //used as default to find closest node.

        /*if a wall is this direction.*/
        bool leftBlock = false;
        bool rightBlock = false;
        bool upBlock = false;
        bool downBlock = false;
        bool done = false;

        /*find closest node.*/
        std::list<PathNode>::iterator currentIt;
        for(std::list<PathNode>::iterator it = openList.begin(); it != openList.end(); ++it)
        {
            if(it->F < min)
            {
                min = it->F;
                current = *it;
                currentIt = it;
            }
        }

        closedList.push_back(PathNode(current.G, current.H,current.index,current.parent));
        openList.erase(currentIt);
        PathNode *currentParent = &closedList.back();

        /*Index of surrounding nodes.*/
        int leftIndex = current.index - 1;
        int rightIndex = current.index + 1;
        int rightObjI = current.index + objwidth;
        int upIndex = current.index - width;
        int downIndex = current.index + width;
        int downObjI = current.index + (objheight * width);

        if(((current.index % width) - 1) < 0 || checkMap(leftIndex,1,objheight,boolMap))
            leftBlock = true; //Left direction blocked.

        if(((current.index % width) + 1) >= width || checkMap(rightObjI,1,objheight,boolMap))
            rightBlock = true; //Right direction blocked.

        if(((current.index / width) - 1) < 0 || checkMap(upIndex,objwidth,1,boolMap))
            upBlock = true; //up direction blocked.

        if(((current.index / width) + 1) >= height || checkMap(downObjI,objwidth,1,boolMap))
            downBlock = true; //down direction blocked.


        if(!leftBlock)
        {
            if(checkDest(leftIndex,destIndex,objwidth,1,width))
                return tracePath(currentParent);

            /*check open and closed list for node.*/

            std::list<PathNode>::iterator oit;
            std::list<PathNode>::iterator cit;

            for(oit = openList.begin(), cit = closedList.begin(); (oit != openList.end() || cit != closedList.end()) && !done; (oit != openList.end())?(++oit):(oit = oit), (cit != closedList.end())?(++cit):(cit = cit))
            {
                if(cit->index == leftIndex)
                    done = true; //Node is on closed list, nothing more to do.

                if(oit->index == leftIndex)
                {
                    if((current.G + 10) < oit->G)
                    {
                        /*shorter path to node, so set current node as parent.*/
                        oit->G = current.G + 10;
                        oit->parent = currentParent;
                    }

                    done = true;
                }
            }

            if(!done)
            {
                /*added to list of nodes to be searched.*/
                openList.push_front(PathNode(current.G + 10, findDistance(leftIndex,destIndex,width), leftIndex, currentParent));
            }

            if(!upBlock)
            {
                done = false;

                int leftUpIndex = leftIndex - width;
                if(checkDest(leftUpIndex,destIndex,objwidth,1,width) || checkDest(leftIndex,destIndex,1,objheight,width))
                    return tracePath(currentParent);

            /*check open and closed list for node.*/
            std::list<PathNode>::iterator oit;
            std::list<PathNode>::iterator cit;

            for(oit = openList.begin(), cit = closedList.begin(); (oit != openList.end() || cit != closedList.end()) && !done; (oit != openList.end())?(++oit):(oit = oit), (cit != closedList.end())?(++cit):(cit = cit))
            {
                if(cit->index == leftUpIndex)
                    done = true; //Node is on closed list, nothing more to do.

                if(oit->index == leftUpIndex)
                {
                    if((current.G + 10) < oit->G)
                    {
                        /*shorter path to node, so set current node as parent.*/
                        oit->G = current.G + 10;
                        oit->parent = currentParent;
                    }

                    done = true;
                }
            }

                if(!done && (!checkMap(leftUpIndex,objwidth,1,boolMap) && !checkMap(leftUpIndex,1,objheight,boolMap)))
                    openList.push_front(PathNode(current.G + 14, findDistance(leftUpIndex,destIndex,width), leftUpIndex, currentParent)); //Add left diagonal node.
            }

            if(!downBlock)
            {
                done = false;

                int leftDownIndex = leftIndex + width;
                int leftDownObjI = leftIndex + (objheight * width);

                if(checkDest(leftDownIndex,destIndex,1,objheight,width) || checkDest(leftDownObjI,destIndex,objwidth,1,width))
                    return tracePath(currentParent);

            /*check open and closed list.*/
            std::list<PathNode>::iterator oit;
            std::list<PathNode>::iterator cit;

            for(oit = openList.begin(), cit = closedList.begin(); (oit != openList.end() || cit != closedList.end()) && !done; (oit != openList.end())?(++oit):(oit = oit), (cit != closedList.end())?(++cit):(cit = cit))
            {
                if(cit->index == leftDownIndex)
                    done = true; //Node is on closed list, nothing more to do.

                if(oit->index == leftDownIndex)
                {
                    if((current.G + 10) < oit->G)
                    {
                        /*shorter path to node, so set current node as parent.*/
                        oit->G = current.G + 10;
                        oit->parent = currentParent;
                    }

                    done = true;
                }
            }

                if(!done && (!checkMap(leftDownIndex,1,objheight,boolMap) && !checkMap(leftDownObjI,objwidth,1,boolMap)))
                    openList.push_front(PathNode(current.G + 14, findDistance(leftDownIndex,destIndex,width), leftDownIndex, currentParent)); //Add left diagonal.
            }
        }

        if(!rightBlock)
        {
            done = false;

            if(checkDest(rightObjI,destIndex,1,objheight,width))
                return tracePath(currentParent);

            /*check open and closed list.*/
            std::list<PathNode>::iterator oit;
            std::list<PathNode>::iterator cit;

            for(oit = openList.begin(), cit = closedList.begin(); (oit != openList.end() || cit != closedList.end()) && !done; (oit != openList.end())?(++oit):(oit = oit), (cit != closedList.end())?(++cit):(cit = cit))
            {
                if(cit->index == rightIndex)
                    done = true; //Node is on closed list, nothing more to do.

                if(oit->index == rightIndex)
                {
                    if((current.G + 10) < oit->G)
                    {
                        /*shorter path to node, so set current node as parent.*/
                        oit->G = current.G + 10;
                        oit->parent = currentParent;
                    }

                    done = true;
                }
            }

            if(!done)
            {
                openList.push_front(PathNode(current.G + 10, findDistance(rightIndex,destIndex,width), rightIndex, currentParent));
            }

            if(!upBlock)
            {
                done = false;

                int rightUpObjI = rightObjI - width;
                int rightUpIndex = rightIndex - width;
                if(checkDest(rightUpIndex,destIndex,objwidth,1,width) || checkDest(rightUpObjI,destIndex,1,objheight,width))
                    return tracePath(currentParent);

            std::list<PathNode>::iterator oit;
            std::list<PathNode>::iterator cit;

            for(oit = openList.begin(), cit = closedList.begin(); (oit != openList.end() || cit != closedList.end()) && !done; (oit != openList.end())?(++oit):(oit = oit), (cit != closedList.end())?(++cit):(cit = cit))
            {
                if(cit->index == rightUpIndex)
                    done = true; //Node is on closed list, nothing more to do.

                if(oit->index == rightUpIndex)
                {
                    if((current.G + 10) < oit->G)
                    {
                        /*shorter path to node, so set current node as parent.*/
                        oit->G = current.G + 10;
                        oit->parent = currentParent;
                    }

                    done = true;
                }
            }

                if(!done && (!checkMap(rightUpIndex,objwidth,1,boolMap) && !checkMap(rightObjI, 1,objheight,boolMap)))
                    openList.push_front(PathNode(current.G + 14, findDistance(rightUpIndex,destIndex,width), rightUpIndex, currentParent));
            }

            if(!downBlock)
            {
                done = false;

                int rightDownIndex = rightIndex + width;
                int rightDownObjI = rightObjI + width;

                if(checkDest(rightDownObjI,destIndex,1,objheight,width) || checkDest((downObjI + 1),destIndex,objwidth,1,width))
                    return tracePath(currentParent);

                std::list<PathNode>::iterator oit;
            std::list<PathNode>::iterator cit;

            for(oit = openList.begin(), cit = closedList.begin(); (oit != openList.end() || cit != closedList.end()) && !done; (oit != openList.end())?(++oit):(oit = oit), (cit != closedList.end())?(++cit):(cit = cit))
            {
                if(cit->index == rightDownIndex)
                    done = true; //Node is on closed list, nothing more to do.

                if(oit->index == rightDownIndex)
                {
                    if((current.G + 10) < oit->G)
                    {
                        /*shorter path to node, so set current node as parent.*/
                        oit->G = current.G + 10;
                        oit->parent = currentParent;
                    }

                    done = true;
                }
            }

                if(!done && (!checkMap(rightDownObjI,1,objheight,boolMap) && !checkMap((downObjI + 1),objwidth,1,boolMap)))
                    openList.push_front(PathNode(current.G + 14, findDistance(rightDownIndex,destIndex,width), rightDownIndex, currentParent));
            }
        }

        if(!upBlock)
        {
            done = false;

            if(checkDest(upIndex,destIndex,objwidth,1,width))
                return tracePath(currentParent);

            std::list<PathNode>::iterator oit;
            std::list<PathNode>::iterator cit;

            for(oit = openList.begin(), cit = closedList.begin(); (oit != openList.end() || cit != closedList.end()) && !done; (oit != openList.end())?(++oit):(oit = oit), (cit != closedList.end())?(++cit):(cit = cit))
            {
                if(cit->index == upIndex)
                    done = true; //Node is on closed list, nothing more to do.

                if(oit->index == upIndex)
                {
                    if((current.G + 10) < oit->G)
                    {
                        /*shorter path to node, so set current node as parent.*/
                        oit->G = current.G + 10;
                        oit->parent = currentParent;
                    }

                    done = true;
                }
            }

            if(!done)
                openList.push_front(PathNode(current.G + 10, findDistance(upIndex,destIndex,width), upIndex, currentParent));
        }

        if(!downBlock)
        {
            done = false;

            if(checkDest(downObjI,destIndex,objwidth,1,width))
                return tracePath(currentParent);

            std::list<PathNode>::iterator oit;
            std::list<PathNode>::iterator cit;

            for(oit = openList.begin(), cit = closedList.begin(); (oit != openList.end() || cit != closedList.end()) && !done; (oit != openList.end())?(++oit):(oit = oit), (cit != closedList.end())?(++cit):(cit = cit))
            {
                if(cit->index == downIndex)
                    done = true; //Node is on closed list, nothing more to do.

                if(oit->index == downIndex)
                {
                    if((current.G + 10) < oit->G)
                    {
                        /*shorter path to node, so set current node as parent.*/
                        oit->G = current.G + 10;
                        oit->parent = currentParent;
                    }

                    done = true;
                }
            }

            if(!done)
                openList.push_front(PathNode(current.G + 10, findDistance(downIndex,destIndex,width), downIndex, currentParent));

        }
    }

    return "E";
}
