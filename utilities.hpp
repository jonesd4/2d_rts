#ifndef UTILITIES
#define UTILITIES

#include<string>
#include<stdlib.h>
#include<sstream>

#include"tinyxml2.h"

const char *getXmlValue(tinyxml2::XMLDocument *, char *);
const char *getXdocValue(char *, char *);
int getXmlInt(tinyxml2::XMLDocument *, char *);
int getXdocInt(char *, char *);
int intFromStr(std::string);
std::string strFromInt(int number);

struct StringIntPair
{
    std::string str;
    int value;
};

int tableSize_StringInt(StringIntPair t[]);

#endif
