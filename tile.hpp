#ifndef TILE
#define TILE

#include<iostream>
#include<list>

#include"EventListener.hpp"
#include"GameStates.hpp"

class IComponent;

/*TODO: Maybe put this in its own file.*/
/*
*Class containing game object information.
*/


class GameObject : public EventListener
{
    public:
        explicit GameObject(int typ, int X, int Y,int ABSX, int ABSY,int W,int H,
                            int img, int rect, int fac, bool draw = false, bool sol = false):
                            type(typ), x(X), y(Y),absX(ABSX),absY(ABSY),width(W),height(H),
                            imgRef(img), imgRect(rect), faction(fac), drawable(draw), solid(sol){} //Constructor initializing values.

        ~GameObject();

        int getType() const {return type;}
        int getImgRef() const{return imgRef;}
        int getImgRect() const{return imgRect;} //Return indext to tile in sheet.
        int getFaction() const{return faction;}
        int getAbsX() const{return absX;}
        int getAbsY() const{return absY;}
        int getX() const{return x;} //Get x position TODO: should use same units.
        int getY() const{return y;} //Get y position TODO: should use same units.
        int getWidth() const{return width;}
        int getHeight() const{return height;}

        bool isDrawable() const{return drawable;} //If the object is drawable.
        bool isSolid() const{return solid;} //If the object is solid
        bool isMoveable() const{return moveable;}
        bool isDead() const{return dead;}

        void setObjType(int ty){type=ty;}
        void setImgRef(int img){imgRef = img;} //Set sheet index.
        void setImgRect(int rect){imgRect = rect;} //Set image rect
        void setFaction(int fac){faction = fac;}
        void setX(int X){x = X;} //Set x position.
        void setY(int Y){y = Y;} //Set y position.
        void setAbsX(int X){absX = X;}
        void setAbsY(int Y){absY = Y;}
        void setWidth(int w){width = w;}
        void setHeight(int h){height = h;}
        void setDrawable(bool b){drawable = b;}
        void setSolid(bool b){solid = b;}
        void setMoveable(bool b){moveable = b;}
        void setDead(bool b){dead = b;}

        void addCom(IComponent *);

        void onEvent(const EVENT_TYPE &, const EventUnion &, const FilterUnion &);
        void onUpdate();

        /*TODO: Again, same units.*/

    private:
        int type;
        int faction;

        int imgRef;
        int imgRect; //tile index.
        int absX;
        int absY;
        int x; //x position.
        int y; //y position.
        int width;
        int height;

        bool moveable;
        bool drawable; //If drawable.
        bool solid; //If solid.
        bool dead;

        std::list<IComponent*> comList;
};

/*
*Class holding information on game position and graphics.
*/
class Tile
{
    public:
        explicit Tile(int img_ref, int rect): imgRef(img_ref), imgRect(rect), gameObj(NULL){} //Constructor initializing values.
        Tile(){} //Default constructor.

        void setImgRef(int img){imgRef = img;} //Set index to sheet.
        void setImgRect(int rect){imgRect = rect;} //Set index to tile.
        void setGameObj(GameObject *go){gameObj = go;} //Set game object on top of tile.

        int getImgRef() const {return imgRef;} //Return index to sheet.
        int getImgRect() const {return imgRect;} //Return index to tile.

        GameObject *getGameObj() const {return gameObj;} //Return pointer to game object on top of tile.

    private:
        int imgRef; //Index to sheet.
        int imgRect; //Index to tile.


        /*TODO: A good bool could be useful.*/

        GameObject *gameObj; //Pointer to game object.
};

#endif
