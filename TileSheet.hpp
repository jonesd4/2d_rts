#ifndef TILESHEET
#define TILESHEET

#include<vector>

#include<SFML\Graphics\Rect.hpp>
#include<SFML\Graphics.hpp>
#include<SFML\Graphics\Texture.hpp>

const int TILESIZE = 32; //Constant tile size.

/*
*Class to hold Texture sheet and rectangles to
*the individual blocks.
*/

struct texData
{
    texData(int s, int tw, int th, int sw, int sh):size(s), tileWidth(tw), tileHeight(th), sheetWidth(sw), sheetHeight(sh){}
    int size;
    int tileWidth;
    int tileHeight;
    int sheetWidth;
    int sheetHeight;
};

class TileSheet
{
    public:
        TileSheet(std::string, int, int, int, int); //Constructor that loads the Tile sheet and builds the rectangles.
        TileSheet(); //Default constructor NOTE: should probably do something with this.
        ~TileSheet(); //Deconstructor.

        void newTileSheet(std::string, int, int, int, int); //Loads a new tile sheet and builds new rectangles.
        void clearSheets();

        sf::Texture const *getImage(int index) const; //Return the SFML texture.
        sf::IntRect const *getRect(int index) const {return &rectArray[index];} //Return the rectangle that holds the block at passed value.

        /*TODO: All units must be the same.*/
        int getTileWidth(int index) const {return texDataArray[index].tileWidth;} //Width of individual tile.
        int getTileHeight(int index) const {return texDataArray[index].tileHeight;} //Height of individual tile.
        int getSheetWidth(int index) const {return texDataArray[index].sheetWidth;} //Width of tile sheet.
        int getSheetHeight(int index)const {return texDataArray[index].sheetHeight;} //Height of tile sheet

        bool isGood() const {return good;} //Good if sheet is loaded.

    private:
        std::vector<sf::Texture> sheetArray; //Tile sheet.
        std::vector<sf::IntRect> rectArray; //Array of rectangles pointing to tiles
        std::vector<texData> texDataArray;

        /*
        int tileWidth; //Individual tile width.
        int tileHeight; //Individual tile height.
        int sheetWidth; //Width of tile sheet.
        int sheetHeight; //Height of tile sheet.
        */

        bool good; //If sheet is loaded.
};

#endif
