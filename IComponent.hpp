#ifndef ICOMPONENT
#define ICOMPONENT

#include"Event.hpp"

class GameObject;

/*
*Component is the base class of all Components that manipulate GameObjects.
*/
class IComponent
{
    public:
        virtual ~IComponent(){} //Empty constructor
        virtual void update(GameObject *){}; //update event for component.
        virtual void onEvent(GameObject *,const EVENT_TYPE &, const EventUnion &, const FilterUnion &){}; //Method to be called in case of event.
};

#endif
