#include<SFML/Window/Event.hpp>

#include"GrphDev_2d.hpp"
#include"TileMap.hpp"
#include"Tile.hpp"
#include"EventHandler.hpp"
#include"Event.hpp"

/*Initialize Graphics values.*/
GrphDev_2d::GrphDev_2d(int swidth,int sheight) : window(sf::VideoMode(swidth,sheight),"Jones_RTS_Test"), screenWidth(swidth),screenHeight(sheight)
{
    screenWidth=swidth;
    screenHeight=sheight;
    window.setFramerateLimit(60); //Set frame rate limit.

    /*Set buffer size*/
    bufferWidth = swidth / TILESIZE + 32;
    bufferHeight = sheight / TILESIZE + 32;
}

void GrphDev_2d::loadSheet(int i,std::string filename, int sheetwidth, int sheetheight, int tilewidth, int tileheight)
{
    /*Load sheet*/
    sheetMap[i].newTileSheet(filename, sheetwidth, sheetheight, tilewidth, tileheight);
}

void GrphDev_2d::clearSheets()
{
    sheetMap.clear(); //Clear Hash map of sheets.
}

void GrphDev_2d::draw(int img,int rect, int x,int y)
{
    /*Draw sprite at position with sheet and tile.*/
    sf::Sprite sprite;
    sprite.setPosition((float)x,(float)y);
    sprite.setTexture(*sheetMap[img].getImage(rect));
    sprite.setTextureRect(*sheetMap[img].getRect(rect));

    window.draw(sprite);
}

void GrphDev_2d::rawDraw(sf::Drawable &obj)
{
    window.draw(obj);
}

void GrphDev_2d::drawObj(GameObject *gobj, int x = 0, int y = 0)
{
    int x_pos = gobj->getX() - x;
    int y_pos = gobj->getY() - y;

    for(int i = 0; i < gobj->getHeight(); i++)
    {
        for(int j = 0; j < gobj->getWidth(); j++)
        {
            int rect = gobj->getImgRect();
            draw(gobj->getImgRef(),(rect + j + (sheetMap[gobj->getImgRef()].getSheetWidth(rect) * i)),x_pos + (j * TILESIZE),y_pos + (i * TILESIZE));
        }
    }
}

void GrphDev_2d::render()
{
    /*Display drawn sprites and clear screen.*/
    window.display();
    window.clear();
}

void GrphDev_2d::drawMap(TileMap *tilemap, int x, int y)
{
    /*Don't draw negative positions.*/
    int x_pos;
    if((x / TILESIZE) < 0)
        x_pos = 0;
    else
        x_pos = x;

    int y_pos;
    if((y / TILESIZE) < 0)
        y_pos = 0;
    else
        y_pos = y;

    /*If map is smaller than screen, change buffer to tile size.*/
    int buffW;
    if(tilemap->getWidth() < bufferWidth)
        buffW =  (x_pos / TILESIZE) + tilemap->getWidth();

    int buffH = bufferHeight;
    if(tilemap->getHeight() < bufferHeight)
        buffH = (y_pos / TILESIZE) + tilemap->getHeight();

    if(buffW > tilemap->getWidth())
        buffW = tilemap->getWidth();

    if(buffH > tilemap->getHeight())
        buffH = tilemap->getHeight();

    /*Draw tiles*/
    for(int i = (y_pos / TILESIZE); i < buffH; i++)
    {
        for(int j = (x_pos / TILESIZE); j < buffW; j++)
        {
            Tile *current = tilemap->get(j,i);

            if(current != NULL)
            {
                /*Calculate tile position.*/
                int tile_x = j * TILESIZE;
                int tile_y = i * TILESIZE;

                draw(current->getImgRef(),current->getImgRect(), tile_x - x_pos, tile_y - y_pos); //Draw tile.

                /*Get game object. If it's drawable, sent to graphics.*/
                GameObject *gobj = current->getGameObj();

                if(gobj != NULL && gobj->isDrawable())
                {
                    ObjectQue.push(gobj);
                }
            }
        }
    }

    while(!ObjectQue.empty())
    {
        drawObj(ObjectQue.front(),x,y);
        ObjectQue.pop();
    }
}

void GrphDev_2d::pollInput()
{
    sf::Event event;

    while(window.pollEvent(event))
    {
        EventUnion eu;

        switch(event.type)
        {
            case sf::Event::Closed:
            {
                FilterUnion fu;
                EventHandler::sendEvent(CLOSE_APP,eu,fu);
                break;
            }

            case sf::Event::KeyPressed:
            {

                switch(event.key.code)
                {

                    case sf::Keyboard::Escape:
                    {
                        FilterUnion fu;
                        EventHandler::sendEvent(CLOSE_APP,eu,fu);
                        break;
                    }
                }
            }
        }
    }
}

Vector2 GrphDev_2d::getMousePos()
{
    sf::Vector2i veci = sf::Mouse::getPosition(window);
    Vector2 vec;
    vec.x = veci.x;
    vec.y = veci.y;
    return vec;
}

void GrphDev_2d::onEvent(const EVENT_TYPE &etype, const EventUnion &eunion)
{
    switch(etype)
    {
        case CLOSE_APP:
        {
            window.close();
        }
    }
}

GrphDev_2d &GrphDev_2d::Graphics(int width, int height)
{
    static GrphDev_2d GraphicsDevice(width,height);
    return GraphicsDevice;
}
