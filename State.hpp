#ifndef BASESTATE
#define BASESTATE

#include"Event.hpp"

enum STATE{LOAD,LOOP,CLOSE}; //Values for states.

/*
*Base class for all game states.
*/
class State
{
    public:
        State(); //Constructor
        virtual ~State(){} //Default virtual deconstructor.
        void run(); //Run current method for state.
        virtual void onLoad(){} //Method during load state.
        virtual void onLoop(){} //Method during loop state.
        virtual void onClose(){} //Method during close state.
        virtual void onEvent(const EVENT_TYPE &, const EventUnion &){} //Method called in case of event.

        STATE getState(){return state;}
        void setState(STATE val){state = val;}

    private:
        STATE state; //Current state of....state. Very articulate...
};

#endif
