#ifndef GRPHDEV_2d
#define GRPHDEV_2d

#include<map>
#include<queue>
#include<SFML/System.hpp>
#include<SFML/Graphics.hpp>
#include<SFML/Window/Mouse.hpp>
#include<SFML/System/Vector2.hpp>

#include"GameStates.hpp"
#include"GameStructs.hpp"
#include"TileSheet.hpp"
#include"EventListener.hpp"
#include"Event.hpp"

class TileMap;
class GameObject;

/*
*Graphics device for THIS game.
*/
class GrphDev_2d : public EventListener
{
    public:
        explicit GrphDev_2d(int,int); //Constructor, pass the screen size.

        void draw(int sheet,int rect,int x,int y); //Draw by passing position, sheet and tile index.
        void rawDraw(sf::Drawable &);
        void drawObj(GameObject *gobj, int x, int y);
        void drawMap(TileMap *tilemap, int x,int y); //Draw map within buffer.
        void loadSheet(int id,std::string filename,int sheetwidth,int sheetheight,int tilewidth,int tileheight); //Load tile sheet
        void clearSheets(); //close all sheets.
        void render(); //Display what has been drawn to window.
        void pollInput();

        Vector2 getMousePos();
        void onEvent(const EVENT_TYPE &, const EventUnion &);

        static GrphDev_2d &Graphics(int width = 600,int height = 600);

    private:

        sf::RenderWindow window; //Screen

        std::map<int,TileSheet> sheetMap; //Hash map from sheet index to sheet.
        std::queue<GameObject *> ObjectQue;

        int screenWidth; //Width of screen.
        int screenHeight; //Height of screen.
        int bufferWidth; //Width of buffer to be drawn
        int bufferHeight; //Height of buffer to be drawn
};

#endif
