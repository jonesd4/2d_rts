#ifndef MAPSTATECOMM
#define MAPSTATECOMM

#include<iostream>
#include<stack>
#include"StateComm.hpp"

class MapState;

class MapStateComm : public StateComm
{
    public:
        MapStateComm(MapState *ms): mapstate(ms){}
        virtual ~MapStateComm(){}

        MapState *getMapState(){return mapstate;}

    private:
        MapState *mapstate;
};

/*
*MapState command to handle the moving of a game object from
*point A to point B
*/
class ObjMoveComm : public MapStateComm
{
    public:
        ObjMoveComm(MapState *, GameObject *, std::string);
        ObjMoveComm(MapState *mapState, GameObject *gameObj, int dest);
        void populatePathStack(std::string);
        void onLoop();
        void onClose();
        void onEvent(const EVENT_TYPE &,const EventUnion &,const FilterUnion &);

    private:
        GameObject *gobj;
        std::stack<int> pathStack;
        int next;
        bool newDest;
        EventUnion savedEU;
        bool hold;
};

class SpawnComm : public MapStateComm
{
    public:
        SpawnComm(MapState *, GameObject *);
        void onLoop();

    private:
        int startX;
        int startY;
        int startWidth;
        int startHeight;
        GameObject *gobj;
};
#endif
