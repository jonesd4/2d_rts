#include"TileMap.hpp"
#include"Tile.hpp"

TileMap::TileMap(): good(false){} //Default constructor.

/*Constructor initializing the map's width and height.*/
TileMap::TileMap(int width_, int height_): width(width_), height(height_)
{
    map = new Tile*[height_]; //Create new array of arrays.

    for(int i = 0; i < height_; i++)
    {
        map[i] = new Tile[width_]; //Create Tiles in internal arrays.
    }

    good = true; //Map safe to use.
}

/*Get Tile at position.*/
Tile *TileMap::get(int x_, int y_)
{
    return &(map[y_][x_]);
}

/*Delete all values if there are any to delete.*/
void TileMap::clear()
{
    if(good)
    {
        for(int i = 0; i < height; i++)
        {
            delete map[i];
        }

    delete map;

    good = false;
    }
}

/*Create new Tile map*/
void TileMap::newMap(int width_, int height_)
{
    clear();

    map = new Tile*[height_];

    for(int i = 0; i < height_; i++)
    {
        map[i] = new Tile[width_];
    }

    good = true;

    width = width_;
    height = height_;
}

/*Set all values of tile at position.*/
void TileMap::newTile(int width,int height,int img,int rect,GameObject *obj = NULL)
{
    map[height][width].setImgRef(img);
    map[height][width].setImgRect(rect);
    map[height][width].setGameObj(obj);
}

TileMap::~TileMap()
{
        clear(); //Clear map on deconstuctor
}
