#ifndef EVENTHANDLER
#define EVENTHANDLER

#include<cstdarg>
#include<list>
#include<map>

#include"Event.hpp"

class EventListener;
class Event;
class GameObject;
class Vector2;

using namespace std;

const int ARG_LIST_END = -1; //constant integer used by EventHandler to recieve multiple arguments.

/*
*Central component of event system.
*/
class EventHandler
{
    public:
        static void subEvent(EVENT_TYPE, EventListener *); //Subscribe Event Listener to one event.
        static void multiSubEvent(EventListener *,...); //Subscribe event listener to multiple events.
        static void sendEvent(const EVENT_TYPE&, const EventUnion&, const FilterUnion &); //Send events to those that have subscribed to them.
        static void sendEvent(EventStruct &);
        static void clear(); //clear hash map event to listener.

    private:
        static map<int,list<EventListener*> > listenerMap; //Hash map event to listener.
};

#endif
