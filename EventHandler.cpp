#include"EventHandler.hpp"

#include"Event.hpp"
#include"EventListener.hpp"
#include"GameStructs.hpp"

map<int,list<EventListener*> > EventHandler::listenerMap; //Declaration of Event listener map.

/*Subscribe event listener to event.*/
void EventHandler::subEvent(EVENT_TYPE index,EventListener *EL)
{
    listenerMap[index].push_front(EL);
}

/*Subscribe event listener to multiple events.*/
void EventHandler::multiSubEvent(EventListener *EL,...)
{
    va_list args;
    va_start(args,EL);

    EVENT_TYPE index = (EVENT_TYPE)va_arg(args,int);

    while(index != ARG_LIST_END)
    {
        subEvent(index,EL);

        index = (EVENT_TYPE)va_arg(args,int);
    }

    va_end(args);
}


/*Set off event*/
void EventHandler::sendEvent(const EVENT_TYPE &etype,const EventUnion &eunion,const FilterUnion &funion)
{
    list<EventListener *>::iterator it;

    for(it = listenerMap[etype].begin(); it != listenerMap[etype].end(); it++)
    {
        (*it)->onEvent(etype, eunion, funion);
    }
}

void EventHandler::sendEvent(EventStruct &es)
{
    sendEvent(es.getEventType(),es.getEventUnion(),es.getFilterUnion());
}

/*delete map values*/
void EventHandler::clear()
{
    map<int,list<EventListener*> >::iterator it;

    for(it = listenerMap.begin(); it != listenerMap.end(); it++)
    {
        list<EventListener*>::iterator lit;

        for(lit = (*it).second.begin(); lit != (*it).second.end(); lit++)
        {
            delete (*lit);
        }
    }

    listenerMap.clear();
}
