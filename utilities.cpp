#include"utilities.hpp"

const char *getXmlValue(tinyxml2::XMLDocument *doc, char *eleName)
{
    tinyxml2::XMLElement *root = doc->RootElement();

    return root->FirstChildElement(eleName)->GetText();
}

const char *getXdocValue(char *filename, char *elename)
{
    tinyxml2::XMLDocument doc;
    doc.LoadFile(filename);

    tinyxml2::XMLElement *root = doc.RootElement();

    return root->FirstChildElement(elename)->GetText();
}

int getXmlInt(tinyxml2::XMLDocument *doc, char *eleName)
{
    return atoi(getXmlValue(doc,eleName));
}

int getXdocInt(char *filename, char *elename)
{
    tinyxml2::XMLDocument doc;
    doc.LoadFile(filename);

    return atoi(getXmlValue(&doc,elename));
}

int intFromStr(std::string str)
{
    const char *cstr = str.c_str();
    return atoi(cstr);
}

std::string strFromInt(int number){
    std::string number_string = "";
    char ones_char;
    int ones = 0;
    while(true){
        ones = number % 10;
        switch(ones){
            case 0: ones_char = '0'; break;
            case 1: ones_char = '1'; break;
            case 2: ones_char = '2'; break;
            case 3: ones_char = '3'; break;
            case 4: ones_char = '4'; break;
            case 5: ones_char = '5'; break;
            case 6: ones_char = '6'; break;
            case 7: ones_char = '7'; break;
            case 8: ones_char = '8'; break;
            case 9: ones_char = '9'; break;
        }
        number -= ones;
        number_string = ones_char + number_string;
        if(number == 0){
            break;
        }
        number = number/10;
    }
    return number_string;
}



int tableSize_StringInt(StringIntPair t[])
{
    int size = 0;

    StringIntPair *table = &t[size];

    while(table->str != "NULL")
    {
        size++;
        table = &t[size];
    }

    return size;
}
