#ifndef LUAGAMECOM
#define LUAGAMECOM

#include"GameCom.hpp"
#include"luawrapper.hpp"

moveCom *moveCom_new(lua_State *L);
attackableCom *attackableCom_new(lua_State *L);
attackerCom *attackerCom_new(lua_State *L);
resourceCom *resourceCom_new(lua_State *L);
builderCom *builderCom_new(lua_State *L);
buildingCom *buildingCom_new(lua_State *L);
animationCom *animationCom_new(lua_State *L);

static int luaopen_moveCom(lua_State *L);
static int luaopen_attackableCom(lua_State *L);
static int luaopen_attackerCom(lua_State *L);
static int luaopen_resourceCom(lua_State *L);
static int luaopen_builderCom(lua_State *L);
static int luaopen_buildingCom(lua_State *L);
static int luaopen_animationCom(lua_State *L);

int luaopen_com(lua_State *);

static luaL_Reg moveCom_table[] = {
    {NULL,NULL}
};
static luaL_Reg moveCom_metatable[] = {
    { NULL,NULL }
};

static luaL_Reg attackableCom_table[] = {
    {NULL,NULL}
};
static luaL_Reg attackableCom_metatable[] = {
    { NULL,NULL }
};

static luaL_Reg attackerCom_table[] = {
    {NULL,NULL}
};
static luaL_Reg attackerCom_metatable[] = {
    { NULL,NULL }
};

static luaL_Reg resourceCom_table[] = {
    {NULL,NULL}
};
static luaL_Reg resourceCom_metatable[] = {
    { NULL,NULL }
};

static luaL_Reg builderCom_table[] = {
    {NULL,NULL}
};
static luaL_Reg builderCom_metatable[] = {
    { NULL,NULL }
};

static luaL_Reg buildingCom_table[] = {
    {NULL,NULL}
};
static luaL_Reg buildingCom_metatable[] = {
    { NULL,NULL }
};

static luaL_Reg animationCom_table[] = {
    {NULL,NULL}
};
static luaL_Reg animationCom_metatable[] = {
    { NULL,NULL }
};

#endif
