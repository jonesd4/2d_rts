#include "animation.hpp"

AnimationSet::AnimationSet()
{
    for(int i = 1; i <= 3; i++)
    {
        aniArray.push_back(Animation());
    }
}

int Animation::next(int& frame)
{
    int next = frame + 1;

    if(next >= (start + frames))
        return start;

    return next;
}

void Animation::set(int start_frame, int num_frames)
{
    start = start_frame;
    frames = num_frames;
}

int AnimationSet::next(int state,int frame)
{
    int next = aniArray[state].next(frame); //PROBE!
    return next;
}

void AnimationSet::setAni(int index, int start, int frames)
{
    aniArray[index].set(start,frames);
}
