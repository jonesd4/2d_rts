#include"UIcom.hpp"
#include"UIobj.hpp"
#include"GrphDev_2d.hpp"
#include"utilities.hpp"

textCountCom::textCountCom(int x,int y,int &r): UIcom(x,y), ref(r)
{
    font.loadFromFile("lifecraft.ttf");
    count.setFont(font);
}

void textCountCom::draw(UIobj *uiobj, GrphDev_2d &GD)
{
    count.setString(strFromInt(ref));
    count.setPosition(uiobj->getX() + getX(),uiobj->getY() + getY());
    count.setColor(sf::Color::White);
    count.setCharacterSize(25);
    GD.rawDraw(count);
}
