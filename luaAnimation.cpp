#include"luaAnimation.hpp"

AnimationSet *AnimationSet_new(lua_State *L)
{
    return new AnimationSet();
}

int luaopen_animation(lua_State *L)
{
    luaW_register<AnimationSet>(L, "AnimationSet", AnimationSet_table, AnimationSet_metatable, AnimationSet_new);
    return 1;
}
