#ifndef BOOLMAP
#define BOOLMAP

/*
*Class to hold boolean values in a board like fashion.
*/
class BoolMap
{
    public:
        BoolMap(int, int); //Creates BoolMap using width and height.
        BoolMap(); //Default constructor

        bool get(int, int) const; //Get bool at x and y position.
        void set(int,int,bool);

        void clear(); //Clears and deletes map.
        void newMap(int, int); //Delete the old map and create a new one.
        void print() const;

        bool isGood() const {return good;} //Is the map usable.

        int getWidth() const {return width;}
        int getHeight() const {return height;}

    private:
        bool **map; //Actual map using double pointers to make array size dynamic.
        bool good; //bool of is the map usable.
        int width; //width of the map.
        int height; //height of the map.
};

#endif
