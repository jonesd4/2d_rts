#ifndef LUAMAPSTATE
#define LUAMAPSTATE

#include"GameStates.hpp"
#include"luawrapper.hpp"

class MapState;

MapState *MapState_new(lua_State *L);

int MapState_makeNewObj(lua_State *L);
int MapState_setObj(lua_State *L);

int luaopen_MapState(lua_State *L);

static luaL_Reg MapState_table[] = {
    {NULL,NULL}
};

static luaL_Reg MapState_metatable[] = {
    {"makeEmptyObj",MapState_makeNewObj},
    {"setObj",MapState_setObj},
    {NULL,NULL}
};

#endif
