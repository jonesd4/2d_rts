#include"utilities.hpp"
#include"GameStates.hpp"
#include"MapStateComm.hpp"
#include"tile.hpp"
#include"TileSheet.hpp"
#include"GameCom.hpp"
#include"PathFinder.hpp"
#include"lua.hpp"

#include<sstream>

void ObjMoveComm::populatePathStack(std::string str)
{
    while(!pathStack.empty())
        pathStack.pop();

    pathStack.push(-1);
    std::stringstream ss;
    ss << str;
    std::string nextstr;
    std::getline(ss,nextstr);

    if(nextstr != "E")
    {
        int probe = intFromStr(nextstr); //PROBE!!!
        while(intFromStr(nextstr) >= 0)
        {
            pathStack.push(intFromStr(nextstr));
            std::getline(ss,nextstr);
            probe = intFromStr(nextstr);
        }
    }
/*
    else
        setDead(true);*/
}

ObjMoveComm::ObjMoveComm(MapState *ms, GameObject *obj, std::string str): MapStateComm(ms), gobj(obj), hold(false),newDest(false), next(-1)
{
    populatePathStack(str);
}

ObjMoveComm::ObjMoveComm(MapState *ms, GameObject *obj, int destIndex): MapStateComm(ms), gobj(obj), hold(false),newDest(false), next(-1)
{
    populatePathStack(PathFinder::findPath(((gobj->getX() / TILESIZE) + ((gobj->getY()/TILESIZE) * ms->getBoolMap().getWidth())),destIndex,ms->getBoolMapAdd(),gobj->getWidth(),gobj->getHeight()));
}

void ObjMoveComm::onLoop()
{
    if(newDest)
    {
        if(gobj->isMoveable())
        {
        populatePathStack(PathFinder::findPath(((gobj->getX() / TILESIZE) + ((gobj->getY()/TILESIZE) * getMapState()->getBoolMap().getWidth())),savedEU.mapintstruct.value,getMapState()->getBoolMapAdd(),gobj->getWidth(),gobj->getHeight()));
        newDest = false;
        hold = false;
        }
    }

    else
    {

        if(!hold)
        {
            next = pathStack.top();
            pathStack.pop();
            hold = true;
        }

        if(next >= 0)
        {
            MapState *ms = getMapState();
            int x = (next % ms->getMapWidth());
            int y = (next / ms->getMapWidth());
            bool flag = true;

            if(!getMapState()->getBoolMap().get(x,y))
            {
                Tile *tile = ms->getTileMap().get(x,y);
                if(tile->getGameObj()!=0 &&tile->getGameObj()!=gobj)
                    flag=false;
            }

            if(flag)
            {
                if(gobj->isMoveable())
                {
                   /* EventUnion eu;
                    eu.vecstruct.vector.x = x;
                    eu.vecstruct.vector.y = y;
                    gobj->onEvent(OBJ_MOVE,eu);
                    hold = false;*/

                    /*NEW CODE*/
                    int playerX = gobj->getX() / TILESIZE;
                    int playerY = gobj->getY() / TILESIZE;
                    int direction;

                    if(x == (playerX - 1))
                    {
                        if(y == (playerY + 1))
                            direction = LEFT_DOWN;

                        else if(y == (playerY - 1))
                            direction = LEFT_UP;

                        else
                            direction = LEFT;
                    }

                    else if(x == playerX)
                    {
                        if(y == (playerY + 1))
                            direction = DOWN;

                        else
                            direction = UP;
                    }

                    else
                    {
                        if(y == (playerY + 1))
                            direction = RIGHT_DOWN;

                        else if(y == (playerY - 1))
                            direction = RIGHT_UP;

                        else
                            direction = RIGHT;
                    }

                    ms->moveObj(gobj,direction);
                    hold = false;
                }
            }
        }

        else
        {
            setState(CLOSE);
        }
    }
}

void ObjMoveComm::onClose()
{
    EventUnion eu;
    FilterUnion fu;
    gobj->onEvent(DEST_REACH,eu,fu);
    setDead(true);
}

void ObjMoveComm::onEvent(const EVENT_TYPE &e, const EventUnion &eu, const FilterUnion &funion)
{
    switch(e)
    {
        case NEW_DEST:
        {
            newDest = true;
            savedEU = eu;
        }
    }
}

SpawnComm::SpawnComm(MapState *ms,GameObject *obj) : MapStateComm(ms),
                                                    gobj(obj),
                                                    startX(obj->getAbsX() - 1),
                                                    startY(obj->getAbsY() - 1),
                                                    startWidth(obj->getWidth() + 2),
                                                    startHeight(obj->getHeight() + 2)
{


}

void SpawnComm::onLoop()
{
    const BoolMap &boolMap = getMapState()->getBoolMap();
    bool done = false;

    for(int i = startY; i < (startY + startHeight) && !done; i++)
    {
        for(int j = startX; j < (startX + startWidth) && !done; j++)
        {
            if(!boolMap.get(j,i))
            {
                lua_State *L = getMapState()->getLuaState();
                std::stringstream ss;
                ss << "UNITS.builder(" << j << "," << i << ");";
                luaL_dostring(L,ss.str().c_str());
                //getMapState()->makeObj(j,i,1,1,0,UNIT);
                done = true;
                EventUnion eu;
                FilterUnion fu;
                gobj->onEvent(SPAWNED,eu,fu);
                setState(CLOSE);
            }
        }
    }
}
