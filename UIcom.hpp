#include<SFML/Graphics.hpp>

class GrphDev_2d;
class UIobj;

class UIcom
{
    public:
        UIcom(int X,int Y) : x(X), y(Y){}
        virtual void draw(UIobj *,GrphDev_2d &){};
        virtual void update(){};
        int getX() const{return x;}
        int getY() const{return y;}

    private:
        int x;
        int y;
};

class textCountCom : public UIcom
{
    public:
        textCountCom(int x,int y,int &r);
        void draw(UIobj *,GrphDev_2d &);

    private:
        int &ref;
        sf::Text count;
        sf::Font font;
};
