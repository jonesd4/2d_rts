#ifndef TILEMAP
#define TILEMAP

class GameObject;
class Tile;

/*
*Class to hold Tiles in a board like fashion.
*/
class TileMap
{
    public:
        TileMap(int, int); //Creates TileMap with the width and height.
        TileMap(); //Default constructor.
        ~TileMap();

        Tile *get(int, int); //Get Tile at x and y position.

        void clear(); //Clears and deletes map.
        void newMap(int, int); //Delete the old map and create a new one.
        void newTile(int,int,int,int,GameObject *);
        int getWidth(){return width;}
        int getHeight(){return height;}

        bool isGood(){return good;}

    private:
        Tile **map; //Actual map using double pointers to make array size dynamic.
        bool good; //bool of is the map usable.
        int width; //width of map in Tile units
        int height; //height of map in Tile units
};

#endif
