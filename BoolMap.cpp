#include<iostream>
#include"BoolMap.hpp"

BoolMap::BoolMap(): good(false){} //Default constructor.

/*Make new bool map*/
BoolMap::BoolMap(int width_, int height_): width(width_), height(height_)
{
    map = new bool*[width_];

    for(int i = 0; i < width_; i++)
    {
        map[i] = new bool[height_];
    }

    good = true;
}

/*Get bool value at position*/
bool BoolMap::get(int x_, int y_) const
{
    if(!(x_ < width) || !(y_ < height) || (x_ < 0) || (y_ < 0))
        return true;

    return map[y_][x_];
}

void BoolMap::set(int x,int y, bool val)
{
    map[y][x] = val;
}

void BoolMap::print() const
{
    for(int i = 0; i < height; i++)
    {
        for(int j = 0; j < width; j++)
        {
            std::cout << map[i][j];
        }

        std::cout << "\n";
    }
    std::cout << "\n";
}

/*clear bool map*/
void BoolMap::clear()
{
    if(good)
    {
        for(int i = 0; i < height; i++)
        {
            delete map[i];
        }

        delete map;

        good = false;
    }
}

/*Create new map.*/
void BoolMap::newMap(int width_, int height_)
{
    clear();

    map = new bool*[height_];

    for(int i = 0; i < height_; i++)
    {
        map[i] = new bool[width_];
    }

    for(int i = 0; i < height_; i++)
    {
        for(int j = 0; j < width_; j++)
        {
            map[i][j] = false;
        }
    }

    good = true;

    width = width_;
    height = height_;
}
