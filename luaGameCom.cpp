#include"luaGameCom.hpp"

moveCom *moveCom_new(lua_State *L)
{
    return new moveCom();
}

attackableCom *attackableCom_new(lua_State *L)
{
    int health = luaL_checknumber(L,1);
    return new attackableCom(health);
}

attackerCom *attackerCom_new(lua_State *L)
{
    return new attackerCom();
}

resourceCom *resourceCom_new(lua_State *L)
{
    return new resourceCom();
}

builderCom *builderCom_new(lua_State *L)
{
    return new builderCom();
}

buildingCom *buildingCom_new(lua_State *L)
{
    return new buildingCom();
}

animationCom *animationCom_new(lua_State *L)
{
    AnimationSet *aniSet = luaW_check<AnimationSet>(L,-1);
    return new animationCom(*aniSet);
}


static int luaopen_moveCom(lua_State *L)
{
    luaW_register<moveCom>(L,"moveCom",moveCom_table,moveCom_metatable,moveCom_new);
    return 1;
}

static int luaopen_attackableCom(lua_State *L)
{
    luaW_register<attackableCom>(L,"attackableCom",attackableCom_table,attackableCom_metatable,attackableCom_new);
    return 1;
}

static int luaopen_attackerCom(lua_State *L)
{
    luaW_register<attackerCom>(L,"attackerCom",attackerCom_table,attackerCom_metatable,attackerCom_new);
    return 1;
}

static int luaopen_resourceCom(lua_State *L)
{
    luaW_register<resourceCom>(L,"resourceCom",resourceCom_table,resourceCom_metatable,resourceCom_new);
    return 1;
}

static int luaopen_builderCom(lua_State *L)
{
    luaW_register<builderCom>(L,"builderCom",builderCom_table,builderCom_metatable,builderCom_new);
    return 1;
}

static int luaopen_buildingCom(lua_State *L)
{
    luaW_register<buildingCom>(L,"buildingCom",buildingCom_table,buildingCom_metatable,buildingCom_new);
    return 1;
}

static int luaopen_animationCom(lua_State *L)
{
    luaW_register<animationCom>(L,"animationCom",animationCom_table,animationCom_metatable,animationCom_new);
    return 1;
}

int luaopen_com(lua_State *L)
{
    luaopen_moveCom(L);
    luaopen_attackerCom(L);
    luaopen_attackableCom(L);
    luaopen_builderCom(L);
    luaopen_buildingCom(L);
    luaopen_resourceCom(L);
    luaopen_animationCom(L);

    return 1;
}
