#include"luaMapState.hpp"
#include"GameStates.hpp"

MapState *MapState_new(lua_State *L)
{
    //DO NOT USE!!!
    return new MapState();
}

int MapState_makeNewObj(lua_State *L)
{
    MapState *ms = luaW_check<MapState>(L,1);

    luaW_push<GameObject>(L,ms->makeEmptyObj());
    return 1;
}

int MapState_setObj(lua_State *L)
{
    MapState *ms = luaW_check<MapState>(L,1);
    GameObject *gobj = luaW_check<GameObject>(L,2);
    ms->setObj(gobj);
    return 0;
}

int luaopen_MapState(lua_State *L)
{
    luaW_register<MapState>(L, "MapState", MapState_table, MapState_metatable, MapState_new);
    return 1;
}
