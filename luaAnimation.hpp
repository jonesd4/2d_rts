#ifndef LUAANIMATION
#define LUAANIMATION

#include"animation.hpp"
#include"luawrapper.hpp"

AnimationSet *AnimationSet_new(lua_State *L);

int luaopen_animation(lua_State *L);

static luaL_Reg AnimationSet_table[] = {
    {NULL,NULL}
};

static luaL_Reg AnimationSet_metatable[] = {
    {NULL,NULL}
};

#endif
