#include"BoundBox.hpp"

const short int BOXSIZE = 32;

void CollisionBox::addStateBox(short int state,
                               short int X,
                               short int Y,
                               short int W,
                               short int H)
{
    StateBoxes[state].push_back( new Box(X,Y,W,H));
}

void CollisionBox::deleteBoxes()
{
    for(vector<list<Box*> >::iterator i = StateBoxes.begin();i != StateBoxes.end();i++)
    {
        for(list<Box*>::iterator j = (*i).begin(); j != (*i).end();j++)
        {
            delete (*j);
        }
    }
}
