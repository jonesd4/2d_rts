#ifndef UIOBJ
#define UIOBJ

#include<list>

class UIcom;
class GrphDev_2d;

class UIobj
{
    public:
        UIobj(int X,int Y,int iref,int irect) : x(X), y(Y), imgRef(iref), imgRect(irect){}
        void draw(GrphDev_2d &);
        void update();
        void addCom(UIcom*);
        int getX() const{return x;}
        int getY() const{return y;}

    private:
        int x;
        int y;
        int imgRef;
        int imgRect;

        std::list<UIcom*> comList;
};
#endif
