#include"GameCom.hpp"
#include"GrphDev_2d.hpp"
#include"GameStates.hpp"
#include"EventHandler.hpp"
#include"Event.hpp"
#include"GameStructs.hpp"
#include"MapStateComm.hpp"

void moveCom::onEvent(GameObject *gobj,const EVENT_TYPE &etype, const EventUnion &eunion, const FilterUnion &funion)
{
    switch(etype)
    {
        case OBJ_MOVE:
        {
            if(!moving)
            {
                gobj->setMoveable(false);
                moving = true;
                offsetX = 0;
                offsetY = 0;

                direction = eunion.intstruct.value;
            }

            break;
        }

        case NEW_DEST:
        {
            if(objmovecomm)
            {
                FilterUnion fu;
                objmovecomm->onEvent(NEW_DEST,eunion,fu);
            }

            else
            {
                objmovecomm = eunion.mapintstruct.mapstate->addComm(new ObjMoveComm(eunion.mapintstruct.mapstate,gobj,eunion.mapintstruct.value));
            }

            break;
        }

        case DEST_REACH:
        {
            objmovecomm = NULL;
            break;
        }
    }
}

void moveCom::update(GameObject *gobj)
{
    if(moving)
    {
        if(offsetX >= 32 || offsetY >= 32)
        {
            gobj->setMoveable(true);
            moving = false;
            offsetX = 0;
            offsetY = 0;
        }

        else
        {
            switch(direction)
            {
                case LEFT:
                {
                    offsetX += 4;
                    gobj->setX(gobj->getX() - 4);
                    break;
                }

                case RIGHT:
                {
                    offsetX += 4;
                    gobj->setX(gobj->getX() + 4);
                    break;
                }

                case UP:
                {
                    offsetY += 4;
                    gobj->setY(gobj->getY() - 4);
                    break;
                }

                case DOWN:
                {
                    offsetY += 4;
                    gobj->setY(gobj->getY() + 4);
                    break;
                }

                case LEFT_UP:
                {
                    offsetX += 4;
                    offsetY += 4;

                    gobj->setX(gobj->getX() - 4);
                    gobj->setY(gobj->getY() - 4);
                    break;
                }

                case LEFT_DOWN:
                {
                    offsetX += 4;
                    offsetY += 4;

                    gobj->setX(gobj->getX() - 4);
                    gobj->setY(gobj->getY() + 4);
                    break;
                }

                case RIGHT_UP:
                {
                    offsetX += 4;
                    offsetY += 4;

                    gobj->setX(gobj->getX() + 4);
                    gobj->setY(gobj->getY() - 4);
                    break;
                }

                case RIGHT_DOWN:
                {
                    offsetX += 4;
                    offsetY += 4;

                    gobj->setX(gobj->getX() + 4);
                    gobj->setY(gobj->getY() + 4);
                    break;
                }
            }
        }
    }
}

void attackableCom::onEvent(GameObject *gobj, const EVENT_TYPE &e_type, const EventUnion &eu, const FilterUnion &funion)
{
    switch(e_type)
    {
        case TAKE_DAMAGE:
        {
            health -= eu.objintstruct.value;

            if(health < 1)
            {
                EventUnion null;
                FilterUnion fu;
                eu.objintstruct.gobj->onEvent(DESTROYED, null, fu);
                gobj->setDead(true);
            }
        }
    }
}

void attackableCom::update(GameObject *gobj)
{

}

void attackerCom::onEvent(GameObject *gobj, const EVENT_TYPE &e_type, const EventUnion &eu, const FilterUnion &funion)
{
    switch(e_type)
    {
        case ATTACK:
        {
            target = eu.objstruct.gobj;
            targetX = target->getAbsX();
            targetY = target->getAbsY();

            EventUnion event;
            FilterUnion fu;
            event.gameobjvecstruct.gameobjvec.gobjOne = gobj;
            event.gameobjvecstruct.gameobjvec.gobjTwo = target;

            EventHandler::sendEvent(ATTACK,event,fu);
            break;
        }

        case DESTROYED:
        {
            target = NULL;
            break;
        }

        case DEST_REACH:
        {
            if(target)
                ready = true;
            break;
        }

        case NEW_DEST:
        {
            if(funion.movefilter == USER_MOVED)
            {
                target = NULL;
                ready = false;
            }
        }
    }
}

void attackerCom::update(GameObject *gobj)
{
    if(target && ready)
    {
        Square sourceSqu(gobj->getAbsX(),gobj->getAbsY(),gobj->getWidth(),gobj->getHeight());
        Square targetSqu(target->getAbsX(),target->getAbsY(),target->getWidth(),target->getHeight());

        if(sourceSqu.overlap(targetSqu))
        {
            EventUnion eu;
            FilterUnion fu;
            eu.objintstruct.value = 1;
            eu.objintstruct.gobj = gobj;

            target->onEvent(TAKE_DAMAGE,eu,fu);
        }

        else
        {
            EventUnion eu;
            FilterUnion fu;
            eu.objstruct.gobj = target;
            onEvent(gobj,ATTACK,eu,fu);
        }
    }
}

void resourceCom::onEvent(GameObject *gobj, const EVENT_TYPE &etype, const EventUnion &eunion, const FilterUnion &funion)
{
    switch(etype)
    {
        case TAKE_DAMAGE:
        {
            health -= eunion.objintstruct.value;

            if(health < 1)
            {
                EventUnion null;
                FilterUnion fu;
                eunion.objintstruct.gobj->onEvent(HARVESTED, null, fu);
                gobj->setDead(true);
            }
        }
    }
}

void resourceCom::update(GameObject *gobj)
{

}

void builderCom::onEvent(GameObject *gobj, const EVENT_TYPE &etype, const EventUnion &eunion, const FilterUnion &funion)
{
    switch(etype)
    {
        case HARVEST:
        {
            target = eunion.objstruct.gobj;

            EventUnion event;
            FilterUnion fu;
            event.gameobjvecstruct.gameobjvec.gobjOne = gobj;
            event.gameobjvecstruct.gameobjvec.gobjTwo = target;

            EventHandler::sendEvent(HARVEST,event,fu);
            break;
        }

        case HARVESTED:
        {
            EventUnion eu;
            FilterUnion fu;
            eu.objstruct.gobj = gobj;
            EventHandler::sendEvent(HARVESTED,eu,fu);
            target = NULL;
            break;
        }

        case DEST_REACH:
        {
            if(target)
                ready = true;
            break;
        }

        case NEW_DEST:
        {
            if(funion.movefilter == USER_MOVED)
            {
                target = NULL;
                ready = false;
            }
            break;
        }
    }
}

void builderCom::update(GameObject *gobj)
{
    if(target && ready)
    {
        Square sourceSqu(gobj->getAbsX(),gobj->getAbsY(),gobj->getWidth(),gobj->getHeight());
        Square targetSqu(target->getAbsX(),target->getAbsY(),target->getWidth(),target->getHeight());

        if(sourceSqu.overlap(targetSqu))
        {
            EventUnion eu;
            FilterUnion fu;
            eu.objintstruct.value = 1;
            eu.objintstruct.gobj = gobj;

            target->onEvent(TAKE_DAMAGE,eu,fu);
        }

        else
        {
            EventUnion eu;
            FilterUnion fu;
            eu.objstruct.gobj = target;
            onEvent(gobj,HARVEST,eu,fu);
        }
    }
}

void buildingCom::onEvent(GameObject *gobj, const EVENT_TYPE &etype, const EventUnion &eunion, const FilterUnion &funion)
{
    switch(etype)
    {
        case SPAWN:
        {
            if(!wait)
            {
                wait = true;
                EventUnion eu;
                eu.objstruct.gobj = gobj;
                FilterUnion fu;
                EventHandler::sendEvent(SPAWN,eu,fu);
            }
            break;
        }

        case SPAWNED:
        {
            wait = false;
            break;
        }
    }
}

void buildingCom::update(GameObject *gobj)
{

}

void animationCom::onEvent(GameObject *gobj, const EVENT_TYPE &etype, const EventUnion &eunion, const FilterUnion &funion)
{
    switch(etype)
    {
        case OBJ_MOVE:
        {
            state = HUMAN_WALK;
            break;
        }

        case DEST_REACH:
        {
            state = HUMAN_IDLE;
            break;
        }
    }
}

void animationCom::update(GameObject *gobj)
{
    count++;

    if(count > delay)
    {
        gobj->setImgRect(aniSet.next(state,gobj->getImgRect()));
        count = 0;
    }
}
