#include"UIobj.hpp"
#include"UIcom.hpp"
#include"GrphDev_2d.hpp"

void UIobj::draw(GrphDev_2d &GD)
{
    GD.draw(imgRef,imgRect,x,y);

    for(std::list<UIcom*>::iterator it = comList.begin(); it != comList.end(); it++)
    {
        (*it)->draw(this,GD);
    }
}

void UIobj::update()
{
    for(std::list<UIcom*>::iterator it = comList.begin(); it != comList.end(); it++)
    {
        (*it)->update();
    }
}

void UIobj::addCom(UIcom *com)
{
    comList.push_back(com);
}
