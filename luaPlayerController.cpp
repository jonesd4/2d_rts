#include"luaPlayerController.hpp"
#include"GameStates.hpp"

PlayerController *PlayerController_new(lua_State *L)
{
    //DO NOT USE!!!
    return new PlayerController();
}

int PlayerController_getFaction(lua_State *L)
{
    PlayerController *ply = luaW_check<PlayerController>(L,1);
    lua_pushnumber(L,ply->getFaction());
    return 1;
}

int luaopen_PlayerController(lua_State *L)
{
    luaW_register<PlayerController>(L, "PlayerController", PlayerController_table, PlayerController_metatable, PlayerController_new);
    return 1;
}
