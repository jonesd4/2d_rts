#include"TileSheet.hpp"
#include<iostream>
#include<math.h>

TileSheet::TileSheet(): good(false){} //If only the default constructor is called, the TileSheet is not good.

TileSheet::~TileSheet()
{
    /*If tile array has values, delete them.*/
    /*
    if(good)
        delete[] rectArray;*/
}

/*Load new tile sheet.*/
void TileSheet::newTileSheet(std::string filename,
                             int sheetwidth,
                             int sheetheight,
                             int tilewidth,
                             int tileheight)
{
    /*Initialize sheet and tile sizes.*/

    /* OLD CODE
    sheetWidth = sheetwidth;
    sheetHeight = sheetheight;
    tileWidth = tilewidth;
    tileHeight = tileheight;
    */

    int tilenum = sheetwidth * sheetheight;

    texDataArray.push_back(texData(tilenum,tilewidth,tileheight,sheetwidth,sheetheight));

    /*If tile array has values, delete them.*/

    /*OLD CODE
    if(good)
        delete[] rectArray;
    */

    //good = false; //Just for safety

    /*Load sheet in Image and then to Texture.*/
    sf::Image img;
    img.loadFromFile(filename);
    img.createMaskFromColor(sf::Color(255,0,255)); //Set color key.
    sheetArray.push_back(sf::Texture());
    sf::Texture &tex = sheetArray.back();

    tex.loadFromImage(img);

    /* OLD CODE
    int tilenum = sheetwidth * sheetheight; //Calculate number of tiles.

    rectArray = new sf::IntRect[tilenum]; //Allocate new array of tiles.
    */

    /*Set tile array values.*/
    for(int i = 0; i < tilenum; i++)
    {
        /*TODO: don't think floor is needed anymore.*/
        rectArray.push_back(sf::IntRect());
        sf::IntRect &ir = rectArray.back();

        float x = floor((i % sheetwidth) * tilewidth);
        float y = floor((i / sheetwidth) * tileheight);

        ir.left = x;
        ir.top = y;

        ir.width = tilewidth;
        ir.height = tileheight;
    }

    //good = true; //Sheet is safe to use.
}

TileSheet::TileSheet(std::string filename, int sheetwidth, int sheetheight, int tilewidth, int tileheight) : good(false)
{
    newTileSheet(filename,sheetwidth,sheetheight,tilewidth,tileheight); //Load sheet using passed values.
}

sf::Texture const *TileSheet::getImage(int index) const
{
    int counter = 0;

    for(int i = 0; i < sheetArray.size(); i++)
    {
        counter += texDataArray[i].size;

        if(index < counter)
        {
            return &sheetArray[i];
        }
    }
}
