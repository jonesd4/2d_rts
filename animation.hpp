#ifndef ANIMATION
#define ANIMATION

#include<vector>

enum HUMAN_ANI_STATE{HUMAN_WALK,HUMAN_IDLE,HUMAN_CAST};
enum PROP_ANI_STATE{PROP_IDLE};

class Animation
{
    public:
        Animation(int str, int frm): frames(frm), start(str){};
        Animation(): start(0), frames(0){};
        int next(int &frame);
        void set(int start_frame,int num_frames);

    private:
        int frames;
        int start;
};

class AnimationSet
{
    public:
        AnimationSet();

        int next(int,int);
        void setAni(int index, int start, int frames);

    private:
        std::vector<Animation> aniArray;
};
#endif
