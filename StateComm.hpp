#ifndef STATECOMM
#define STATECOMM

#include"State.hpp"
#include"Event.hpp"

class StateComm
{
    public:
        StateComm();
        virtual ~StateComm(){}
        void run();
        virtual void onLoad(){setState(LOOP);}
        virtual void onLoop(){setState(CLOSE);}
        virtual void onClose(){setDead(true);}
        virtual void onEvent(const EVENT_TYPE &, const EventUnion &, const FilterUnion &){}

        STATE getState(){return state;}
        void setState(STATE s){state = s;}

        bool isDead(){return dead;}
        void setDead(bool d){dead = d;}

    private:
        bool dead;
        STATE state;
};

#endif
