#include<iostream>
#include<stdlib.h>
#include<fstream>
#include<sstream>

#include"GameStates.hpp"

#include"luaGameObject.hpp"
#include"luaGameCom.hpp"
#include"luaEventStruct.hpp"
#include"luaMapState.hpp"
#include"luaAnimation.hpp"
#include"luaPlayerController.hpp"

#include<SFML/Graphics/Texture.hpp>
#include<SFML/Window/Keyboard.hpp>
#include<SFML/Window/Event.hpp>
#include<SFML/Window/Mouse.hpp>

#include"MapStateComm.hpp"
#include"EventHandler.hpp"
#include"GrphDev_2d.hpp"
#include"PathFinder.hpp"
#include"ProcessManager.hpp"
#include"tinyxml2.h"
#include"UIcom.hpp"
#include"Tile.hpp"



void PlayerController::update(MapState *ms)
{
        GrphDev_2d &GD = GrphDev_2d::Graphics();

        GD.pollInput();

        Vector2 vec = GD.getMousePos();
        int cursorX = (vec.x + ms->getX()) / TILESIZE;
        int cursorY = (vec.y + ms->getY()) / TILESIZE;

        if(cursorX < 0)
            cursorX = 0;

        if(cursorY < 0)
            cursorY = 0;

        TileMap &tileMap = ms->getTileMap();
        BoolMap &boolMap = ms->getBoolMap();

        if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            if(!mousehold)
            {
                if(selected == NULL)
                {
                    Tile *tile = tileMap.get(cursorX,cursorY);
                    selected = tile->getGameObj();
                    mousehold = true;
                }

                else
                {
                    /*OLD CODE*/
                    /*commandList.push_back(new ObjMoveComm(this,selected,PathFinder::findPath(((selected->getX() / TILESIZE) + ((selected->getY()/TILESIZE) * boolMap.getWidth())), (cursorX + (cursorY * boolMap.getWidth())),&boolMap,selected->getWidth(),selected->getHeight())));
                    mousehold = true;
                    boolMap.print();*/

                    /*NEW CODE*/
                    Tile *tile = tileMap.get(cursorX,cursorY);

                    if(tile->getGameObj() == NULL)
                    {
                        EventUnion eu;
                        FilterUnion fu;
                        fu.movefilter = USER_MOVED;
                        eu.mapintstruct.mapstate = ms;
                        eu.mapintstruct.value = (cursorX) + (cursorY * boolMap.getWidth());
                        selected->onEvent(NEW_DEST,eu,fu);
                    }

                    else
                    {
                        GameObject *gobj = tile->getGameObj();

                        if(gobj->getType() == UNIT)
                        {
                            if(gobj->getFaction() != faction)
                            {
                                EventUnion eu;
                                FilterUnion fu;
                                eu.objstruct.gobj = gobj;
                                selected->onEvent(ATTACK,eu,fu);
                            }

                            else
                            {
                                selected = gobj;
                            }

                        }

                        else if(gobj->getType() == RESOURCE)
                        {
                            EventUnion eu;
                            FilterUnion fu;
                            eu.objstruct.gobj = gobj;
                            selected->onEvent(HARVEST,eu,fu);
                        }

                        else if(gobj->getType() == BUILDING)
                        {
                            if(recs > 0)
                            {
                                EventUnion eu;
                                FilterUnion fu;
                                recs--;
                                gobj->onEvent(SPAWN,eu,fu);
                            }
                        }
                    }
                }
            }

            mousehold = true;
        }

        else
            mousehold = false;

}

void MapState::registerTypeMap(StringIntPair t[])
{
    int tableSize = tableSize_StringInt(t);

    for(int i = 0; i < tableSize; i++)
    {
        typeIntMap[std::string(t[i].str)] = t[i].value;
    }
}

void MapState::registerComMap(StringIntPair t[])
{
    int tableSize = tableSize_StringInt(t);

    for(int i = 0; i < tableSize; i++)
    {
        int probe = t[i].value;
        std::string probeStr = t[i].str;
        comIntMap[std::string(t[i].str)] = t[i].value;
    }
}

void MapState::registerAniMap(StringIntPair t[])
{
    int tableSize = tableSize_StringInt(t);
    for(int i = 0; i < tableSize; i++)
    {
        int probe = t[i].value;
        std::string probeStr = t[i].str;
        aniIntMap[std::string(t[i].str)] = t[i].value;
    }
}

void MapState::registerFacMap(StringIntPair t[])
{
    int tableSize = tableSize_StringInt(t);

    for(int i = 0; i < tableSize; i++)
    {
        facIntMap[std::string(t[i].str)] = t[i].value;
    }
}

void MapState::makeObj(tinyxml2::XMLElement *obj)
{
    int x_coor = obj->IntAttribute("x_coor");
    int y_coor = obj->IntAttribute("y_coor");
    int width = obj->IntAttribute("width");
    int height = obj->IntAttribute("height");
    int rect = obj->IntAttribute("img");
    int faction = obj->IntAttribute("faction");
    int type = typeIntMap[obj->Value()];

    switch(type)
    {
        case SOLID:
        {
            gameObjList.push_back(GameObject(type,x_coor * TILESIZE,y_coor * TILESIZE, x_coor, y_coor, width, height, OBJECT, NULL, faction,false, true));
            GameObject *gb = &gameObjList.back();

            setObj(gb);

            /*
            for(int i = 0; i < height; i++)
            {
                for(int j = 0; j < width; j++)
                {
                    Tile *tile = tileMap.get((x_coor + (1*j)),(y_coor + (1*i)));
                    tile->setGameObj(&gameObjList.back());
                    boolMap.set((x_coor + (1*j)),(y_coor + (1*i)),true);
                }
            }
            */
            break;
        }

        case BOX:
        {
            gameObjList.push_back(GameObject(type,x_coor * TILESIZE,y_coor * TILESIZE,x_coor,y_coor,width,height,OBJECT,rect,faction,true,true));
            GameObject *gb = &gameObjList.back();

            setObj(gb);

            /*
            for(int i = 0; i < height; i++)
            {
                for(int j = 0; j < width; j++)
                {
                    Tile *tile = tileMap.get((x_coor + (1*j)),(y_coor + (1*i)));
                    tile->setGameObj(&gameObjList.back());
                    boolMap.set((x_coor + (1*j)),(y_coor + (1*i)),true);
                }
            }*/

            break;
        }

        case UNIT:
        {
            gameObjList.push_back(GameObject(type,x_coor * TILESIZE,y_coor * TILESIZE,x_coor,y_coor,width,height,OBJECT,rect,faction,true,true));
            GameObject *gb = &gameObjList.back();
            gb->setMoveable(true);

            /*
            gb->addCom(new moveCom());
            gb->addCom(new attackableCom(100));
            gb->addCom(new attackerCom());
            gb->addCom(new builderCom());
            gb->addCom(new animationCom(aniSetMap["test"]));
            */

            setObj(gb);

            /*
            for(int i = 0; i < height; i++)
            {
                for(int j = 0; j < width; j++)
                {
                    Tile *tile = tileMap.get((x_coor + (1*j)),(y_coor + (1*i)));
                    tile->setGameObj(gb);
                    boolMap.set((x_coor + (1*j)),(y_coor + (1*i)),true);
                }
            }
            */
            break;
        }

        case RESOURCE:
        {
            gameObjList.push_back(GameObject(type,x_coor * TILESIZE, y_coor * TILESIZE,x_coor,y_coor,width,height,OBJECT,rect,faction,true,true));
            GameObject *gb = &gameObjList.back();
            gb->setMoveable(true);

            //gb->addCom(new resourceCom());
            setObj(gb);

            /*
            for(int i = 0; i < height; i++)
            {
                for(int j = 0; j < width; j++)
                {
                    Tile *tile = tileMap.get((x_coor + (1*j)),(y_coor + (1*i)));
                    tile->setGameObj(&gameObjList.back());
                    boolMap.set((x_coor + (1*j)),(y_coor + (1*i)),true);
                }
            }
            */

            break;
        }

        case BUILDING:
        {
            gameObjList.push_back(GameObject(type,x_coor * TILESIZE, y_coor * TILESIZE,x_coor,y_coor,width,height,OBJECT,rect,faction,true,true));
            GameObject *gb = &gameObjList.back();
            gb->setMoveable(false);
            //gb->addCom(new buildingCom());

            setObj(gb);
            /*
            for(int i = 0; i < height; i++)
            {
                for(int j = 0; j < width; j++)
                {
                    Tile *tile = tileMap.get((x_coor + (1*j)),(y_coor + (1*i)));
                    tile->setGameObj(&gameObjList.back());
                    boolMap.set((x_coor + (1*j)),(y_coor + (1*i)),true);
                }
            }
            */
            break;
        }
    }

    GameObject *gobj = &gameObjList.back();

    tinyxml2::XMLElement *currcom = obj->FirstChildElement();

    while(currcom != NULL)
    {
        int com_type = comIntMap[currcom->Value()];

        switch(com_type)
        {
            case MOVECOM:
            {
                gobj->addCom(new moveCom());
                break;
            }

            case ATTACKABLECOM:
            {
                gobj->addCom(new attackableCom(currcom->IntAttribute("health")));
                break;
            }

            case ATTACKERCOM:
            {
                gobj->addCom(new attackerCom());
                break;
            }

            case RESOURCECOM:
            {
                gobj->addCom(new resourceCom());
                break;
            }

            case BUILDERCOM:
            {
                gobj->addCom(new builderCom());
                break;
            }

            case BUILDINGCOM:
            {
                gobj->addCom(new buildingCom());
                break;
            }

            case ANIMATIONCOM:
            {
                gobj->addCom(new animationCom(aniSetMap[currcom->Attribute("animation")]));
                break;
            }
        }

        currcom = currcom->NextSiblingElement();
    }
}

void MapState::setObj(GameObject *gobj)
{
    for(int i = 0; i < gobj->getHeight(); i++)
    {
        for(int j = 0; j < gobj->getWidth(); j++)
        {
            Tile *tile = tileMap.get((gobj->getAbsX() + (1*j)),(gobj->getAbsY() + (1*i)));
            tile->setGameObj(gobj);
            boolMap.set((gobj->getAbsX() + (1*j)),(gobj->getAbsY() + (1*i)),true);
        }
    }
}

GameObject *MapState::makeEmptyObj()
{
    gameObjList.push_back(GameObject(UNIT,0,0,0,0,0,0,OBJECT,0,NONE,true,true));
    return &gameObjList.back();
}

MapState::MapState(char *mapfilename): x(0), y(0), mapWidth(0), mapHeight(0), recs(0), L(lua_open())
{
    EventHandler::subEvent(INPUT,this);
    EventHandler::subEvent(CLOSE_APP,this);
    EventHandler::subEvent(ATTACK,this);
    EventHandler::subEvent(HARVEST,this);
    EventHandler::subEvent(HARVESTED,this);
    EventHandler::subEvent(SPAWN,this);

    /*Load xml file*/
    tinyxml2::XMLDocument doc;
    doc.LoadFile(mapfilename);

    registerTypeMap(ObjTypeTable);
    registerComMap(ComTable);
    registerAniMap(AniTypeTable);

    /*
    typeIntMap["SOLID"] = SOLID;
    typeIntMap["BOX"] = BOX;
    typeIntMap["UNIT"] = UNIT;
    typeIntMap["RESOURCE"] = RESOURCE;
    typeIntMap["BUILDING"] = BUILDING;*/

    tinyxml2::XMLElement *mapEle = doc.FirstChildElement("Map"); //Get map root.
    tinyxml2::XMLElement *mapSheet = mapEle->FirstChildElement("MapSheet"); //Get map sheet info.
    tinyxml2::XMLElement *mapData = mapEle->FirstChildElement("MapData"); //Get map data.
    tinyxml2::XMLElement *tileSheets = mapEle->FirstChildElement("TileSheets"); //Get element that holds tile sheet info.
    tinyxml2::XMLElement *objects = mapEle->FirstChildElement("Objects"); //Get element that holds object info.
    tinyxml2::XMLElement *animations = mapEle->FirstChildElement("Animations"); //Get element that holds animation info.

    mapWidth = mapData->IntAttribute("width");
    mapHeight = mapData->IntAttribute("height");

    /*Make new tile and bool map.*/
    tileMap.newMap(mapWidth,mapHeight);
    boolMap.newMap(mapWidth,mapHeight);

    /*Get tile sheet and tile map size*/

    GrphDev_2d &GD = GrphDev_2d::Graphics();

    std::string mapFile = mapSheet->GetText();
    int tileMapW = mapSheet->IntAttribute("width");
    int tileMapH = mapSheet->IntAttribute("height");

    GD.loadSheet(MAP,mapFile,tileMapW,tileMapH,32,32); //Load tile sheet

    /*Set tile index using file*/
    fstream file;
    file.open(mapData->GetText());
    for(int i = 0; i < mapHeight; i++)
    {
        for(int j = 0; j < mapWidth; j++)
        {
            Tile *tile = tileMap.get(j,i);

            tile->setImgRef(MAP);
            tile->setGameObj(NULL);

            int rect;
            file >> rect;
            tile->setImgRect(rect);
        }
    }

    luaL_openlibs(L);
    luaopen_GameObject(L);
    luaopen_com(L);
    luaopen_EventStruct(L);
    luaopen_MapState(L);
    luaopen_animation(L);
    luaopen_PlayerController(L);

    lua_register(L,"sendEvent",lua_sendEvent);
    //luaL_dofile(L,"enum.lua");
    luaL_dofile(L,"config.lua");

    tinyxml2::XMLElement *currSheet = tileSheets->FirstChildElement();
    /*NOTE: Find how to iterate through XML children.*/
    while(currSheet != NULL)
    {
        GD.loadSheet(currSheet->IntAttribute("index"),
                      currSheet->GetText(),
                      currSheet->IntAttribute("width"),
                      currSheet->IntAttribute("height"),
                      currSheet->IntAttribute("tilewidth"),
                      currSheet->IntAttribute("tileheight"));

        currSheet = currSheet->NextSiblingElement();
    }

    lua_getglobal(L,"ANIMATION_SET");

    player.setFaction(P1);
    player.setRecs(0);

    tinyxml2::XMLElement *currAniSet = animations->FirstChildElement();
    while(currAniSet != NULL)
    {
        std::string name = currAniSet->Attribute("name");

        tinyxml2::XMLElement *currAni = currAniSet->FirstChildElement();
        while(currAni != NULL)
        {
            int start = currAni->IntAttribute("start");
            int frames = currAni->IntAttribute("frames");
            int probe = aniIntMap[currAni->Value()];
            aniSetMap.insert(std::pair<std::string,AnimationSet>(name,AnimationSet()));
            aniSetMap[name].setAni(aniIntMap[currAni->Value()],start,frames);

            currAni = currAni->NextSiblingElement();
        }

        lua_pushstring(L,name.c_str());
        luaW_push<AnimationSet>(L,&aniSetMap[name]);
        lua_settable(L,-3);
        //lua_pop(L,3);

        /*
        int start = currAni->IntAttribute("start");
        int frames = currAni->IntAttribute("frames");
        aniSetMap.insert(std::pair<std::string,AnimationSet>(name,AnimationSet()));
        aniSetMap[name].setAni(HUMAN_IDLE,start,frames);
        */

        currAniSet = currAniSet->NextSiblingElement();
    }

    tinyxml2::XMLElement *currObj = objects->FirstChildElement();
    while(currObj != NULL)
    {
        makeObj(currObj);

        //makeObj(currObj->IntAttribute("x_coor"),currObj->IntAttribute("y_coor"),currObj->IntAttribute("width"),currObj->IntAttribute("height"),currObj->IntAttribute("img"),currObj->Value());

        currObj = currObj->NextSiblingElement();
    }

    uiList.push_back(UIobj(0,0,UI,0));
    UIobj *count = &uiList.back();
    count->addCom(new textCountCom(0,0,player.getRecs()));

    //Setting bool map edges to false;
    for(int i = 0; i < mapWidth; i++)
    {
        boolMap.set(i,0,true);
        boolMap.set(i,(mapHeight - 1),true);
    }

    for(int i = 0; i < mapHeight; i++)
    {
        boolMap.set(0,i,true);
        boolMap.set((mapWidth -1),i,true);
    }

    luaW_push<MapState>(L,this);
    lua_setglobal(L,"mapstate");

    luaL_dofile(L,"test.lua");

    //std::cout << luaL_checkstring(L,-1);


    //Testing ObjMoveCommand
    //int index = (player->getX()/TILESIZE) + ((player->getY()/TILESIZE) * mapWidth);
    //commandList.push_back(new ObjMoveComm(this,player,PathFinder::findPath(index,121,&boolMap,player->getWidth(),player->getHeight())));
}

void MapState::moveObj(GameObject *gobj, int direction)
{
    if(gobj->isMoveable())
    {

        int x = gobj->getAbsX();
        int y = gobj->getAbsY();
        bool moveFlag = true;

        switch(direction)
        {
            case LEFT:
            {
                /*if((x - 1) < 0)
                    moveFlag = false;

                if(moveFlag)
                {
                    for(int i = 0; i < gobj->getHeight() && moveFlag; i++)
                    {
                        bool flag = boolMap.get((x - 1), (y + i));
                        if(flag)
                            moveFlag = false;
                    }
                }*/
                x -= 1;

                //gobj.setAbsX(gobj.getAbsX() - 1);

                break;
            }

            case RIGHT:
            {
                /*if(((x + 1) + gobj->getWidth()) > mapWidth)
                    moveFlag = false;

                if(moveFlag)
                {
                    for(int i = 0; i < gobj->getHeight() && moveFlag; i++)
                    {
                        bool flag = boolMap.get((x + gobj->getWidth()), (y + i));
                        if(flag)
                            moveFlag = false;
                    }
                }*/
                x += 1;

                //gobj.setAbsX(gobj.getAbsX() + 1);

                break;
            }

            case UP:
            {
                /*if((y - 1) < 0)
                    moveFlag = false;

                if(moveFlag)
                {
                    for(int i = 0; i < gobj->getWidth() && moveFlag; i++)
                    {
                        bool flag = boolMap.get((x + i),(y - 1));
                        if(flag)
                            moveFlag = false;
                    }
                }*/
                y -= 1;

                //gobj.setAbsY(gobj.getAbsY() - 1);

                break;
            }

            case DOWN:
            {
                /*if(((y + 1) + gobj->getHeight()) > mapHeight)
                    moveFlag = false;

                if(moveFlag)
                {
                    for(int i = 0; i < gobj->getWidth() && moveFlag; i++)
                    {
                        bool flag = boolMap.get((x + i),(y + gobj->getHeight()));
                        if(flag)
                            moveFlag = false;
                    }
                }*/
                y += 1;

                //gobj.setAbsY(gobj.getAbsY() + 1);

                break;
            }

            case LEFT_UP:
            {
                /*if((x - 1) < 0 || (y - 1) < 0)
                    moveFlag = false;

                if(moveFlag)
                {
                    for(int i = 0; i < gobj->getWidth() && moveFlag; i++)
                    {
                        bool flag = boolMap.get((x + i),(y + gobj->getHeight()));
                        if(flag)
                            moveFlag = false;
                    }
                }*/
                x -= 1;
                y -= 1;
                //gobj.setAbsX(gobj.getAbsX() - 1);
                //gobj.setAbsY(gobj.getAbsY() - 1);

                break;
            }

            case RIGHT_UP:
            {
                x += 1;
                y -= 1;
                //gobj.setAbsX(gobj.getAbsX() + 1);
                //gobj.setAbsY(gobj.getAbsY() - 1);
                break;
            }

            case LEFT_DOWN:
            {
                x -= 1;
                y += 1;
                //gobj.setAbsX(gobj.getAbsX() - 1);
                //gobj.setAbsY(gobj.getAbsY() + 1);

                break;
            }

            case RIGHT_DOWN:
            {
                x += 1;
                y += 1;
                //gobj.setAbsX(gobj.getAbsX() + 1);
                //gobj.setAbsY(gobj.getAbsY() + 1);
                break;
            }
        }

        if(moveFlag)
        {
            for(int i = gobj->getAbsY(); i < (gobj->getAbsY() + gobj->getHeight()); i++)
            {
                for(int j = gobj->getAbsX(); j < (gobj->getAbsX() + gobj->getWidth()); j++)
                {
                    Tile *tile = tileMap.get(j,i);
                    tile->setGameObj(NULL);
                    boolMap.set(j,i,false);
                }
            }

            for(int i = y; i < (y + gobj->getHeight()); i++)
            {
                for(int j = x; j < (x + gobj->getWidth()); j++)
                {
                    Tile *tile = tileMap.get(j,i);
                    tile->setGameObj(gobj);
                    boolMap.set(j,i,true);
                }
            }

            gobj->setAbsX(x);
            gobj->setAbsY(y);

            EventUnion eu;
            FilterUnion fu;
            eu.intstruct.value = direction;
            gobj->onEvent(OBJ_MOVE,eu,fu);

            //boolMap.print();
        }
    }
}

void MapState::updateComm()
{
    for(std::list<MapStateComm*>::iterator it = commandList.begin(); it != commandList.end(); it++)
    {
        if(!(*it)->isDead())
            (*it)->run();
        else
        {
            delete (*it);
            commandList.erase(it++);
        }
    }
}

void MapState::updateGameObj()
{
    lua_getglobal(L,"clearGameObjects");
    lua_call(L,0,0);

    int count = 0;
    lua_getglobal(L,"GAME_OBJECTS");

    for(std::list<GameObject>::iterator it = gameObjList.begin(); it != gameObjList.end(); it++)
    {
        if(!(*it).isDead())
        {
            (*it).onUpdate();
            lua_pushnumber(L,count);
            luaW_push<GameObject>(L,&(*it));
            lua_settable(L,-3);
            count++;
        }

        else
        {
            int width = (*it).getWidth();
            int height = (*it).getHeight();
            int x = (*it).getAbsX();
            int y = (*it).getAbsY();

            for(int i = y; i < (y + height); i++)
            {
                for(int j = x; j < (x + width); j++)
                {
                    Tile *tile = tileMap.get(j,i);
                    tile->setGameObj(NULL);
                    boolMap.set(j,i,false);
                }
            }

            gameObjList.erase(it++);
        }
    }
}

void MapState::updateUIobj()
{
    for(std::list<UIobj>::iterator it = uiList.begin(); it != uiList.end(); it++)
    {
        (*it).update();
    }
}

void MapState::drawUI()
{
    GrphDev_2d &GD = GrphDev_2d::Graphics();

    for(std::list<UIobj>::iterator it = uiList.begin(); it != uiList.end(); it++)
    {
        (*it).draw(GD);
    }
}

MapStateComm *MapState::addComm(MapStateComm *comm)
{
    commandList.push_back(comm);
    return comm;
}

void MapState::onLoad()
{
    setState(LOOP);
}

void MapState::onLoop()
{
        GrphDev_2d &GD = GrphDev_2d::Graphics();

        /*
        GD.pollInput();

        Vector2 vec = GD.getMousePos();
        cursorX = (vec.x + x) / TILESIZE;
        cursorY = (vec.y + y) / TILESIZE;

        if(cursorX < 0)
            cursorX = 0;

        if(cursorY < 0)
            cursorY = 0;

        if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            if(!mousehold)
            {
                if(selected == NULL)
                {
                    Tile *tile = tileMap.get(cursorX,cursorY);
                    selected = tile->getGameObj();
                    mousehold = true;
                }

                else
                {
                    /*OLD CODE*/
                    /*commandList.push_back(new ObjMoveComm(this,selected,PathFinder::findPath(((selected->getX() / TILESIZE) + ((selected->getY()/TILESIZE) * boolMap.getWidth())), (cursorX + (cursorY * boolMap.getWidth())),&boolMap,selected->getWidth(),selected->getHeight())));
                    mousehold = true;
                    boolMap.print();*/

                    /*NEW CODE*//*
                    Tile *tile = tileMap.get(cursorX,cursorY);

                    if(tile->getGameObj() == NULL)
                    {
                        EventUnion eu;
                        FilterUnion fu;
                        fu.movefilter = USER_MOVED;
                        eu.mapintstruct.mapstate = this;
                        eu.mapintstruct.value = (cursorX) + (cursorY * boolMap.getWidth());
                        selected->onEvent(NEW_DEST,eu,fu);
                    }

                    else
                    {
                        GameObject *gobj = tile->getGameObj();

                        if(gobj->getType() == UNIT)
                        {
                            EventUnion eu;
                            FilterUnion fu;
                            eu.objstruct.gobj = gobj;
                            selected->onEvent(ATTACK,eu,fu);
                        }

                        if(gobj->getType() == RESOURCE)
                        {
                            EventUnion eu;
                            FilterUnion fu;
                            eu.objstruct.gobj = gobj;
                            selected->onEvent(HARVEST,eu,fu);
                        }

                        if(gobj->getType() == BUILDING)
                        {
                            EventUnion eu;
                            FilterUnion fu;
                            gobj->onEvent(SPAWN,eu,fu);
                        }
                    }
                }
            }

            mousehold = true;
        }

        else
            mousehold = false;*/

        player.update(this);


        if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
            x -= 3;

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
            x += 3;

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
            y -= 3;

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
            y += 3;


        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
            moveObj(player.getSelected(),LEFT);

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
            moveObj(player.getSelected(),RIGHT);

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
            moveObj(player.getSelected(),UP);

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
            moveObj(player.getSelected(),DOWN);

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
            player.setSelected(NULL);


        if(x < 0)
            x = 0;

        if(y < 0)
            y = 0;

        if((x + 600) > 736)
            x = 736 - 600;

        if((y + 600) > 736)
            y = 736 - 600;

        /*if(selected)
            selected->onUpdate();*/

        //player->onUpdate();
        GD.drawMap(&tileMap,x,y);
        drawUI();
        GD.render();

        updateGameObj();
        updateComm();
        updateUIobj();
        lua_getglobal(L,"main");
        lua_call(L,0,0);
}

void MapState::onClose()
{
    ProcessManager &PM = ProcessManager::ProcessMan();
    PM.popState();
}

void MapState::onEvent(const EVENT_TYPE &etype, const EventUnion &eunion, const FilterUnion &funion)
{
    switch(etype)
    {
        case INPUT:
        {
            switch(eunion.intstruct.value)
            {
                case sf::Keyboard::Left:
                {
                    moveObj(player.getSelected(),LEFT);
                    break;
                }

                case sf::Keyboard::Right:
                {
                    moveObj(player.getSelected(),RIGHT);
                    break;
                }

                case sf::Keyboard::Up:
                {
                    moveObj(player.getSelected(),UP);
                    break;
                }

                case sf::Keyboard::Down:
                {
                    moveObj(player.getSelected(),DOWN);
                    break;
                }

                case sf::Keyboard::A:
                {
                    x -= 3;
                    break;
                }

                case sf::Keyboard::D:
                {
                    x += 3;
                    break;
                }

                case sf::Keyboard::W:
                {
                    y -= 3;
                    break;
                }

                case sf::Keyboard::S:
                {
                    y += 3;
                    break;
                }
            }

            break;
        }

        case ATTACK:
        {
            EventUnion eu;
            FilterUnion fu;
            fu.movefilter = ATTACK_MOVED;
            int dest = eunion.gameobjvecstruct.gameobjvec.gobjTwo->getAbsX() + (eunion.gameobjvecstruct.gameobjvec.gobjTwo->getAbsY() * boolMap.getWidth());
            eu.mapintstruct.mapstate = this;
            eu.mapintstruct.value = dest;
            eunion.gameobjvecstruct.gameobjvec.gobjOne->onEvent(NEW_DEST,eu,fu);
            break;
        }

        case HARVEST:
        {
            EventUnion eu;
            FilterUnion fu;
            fu.movefilter = ATTACK_MOVED;
            int dest = eunion.gameobjvecstruct.gameobjvec.gobjTwo->getAbsX() + (eunion.gameobjvecstruct.gameobjvec.gobjTwo->getAbsY() * boolMap.getWidth());
            eu.mapintstruct.mapstate = this;
            eu.mapintstruct.value = dest;
            eunion.gameobjvecstruct.gameobjvec.gobjOne->onEvent(NEW_DEST,eu,fu);
            break;
        }

        case HARVESTED:
        {
            GameObject *gobj = eunion.objstruct.gobj;
            if(gobj->getFaction() == P1)
            {
                player.setRecs(player.getRecs() + 1);
            }

            break;
        }

        case SPAWN:
        {
            addComm(new SpawnComm(this,eunion.objstruct.gobj));
            break;
        }

        case CLOSE_APP:
        {
            setState(CLOSE);
            break;
        }
    }
}
