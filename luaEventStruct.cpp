#include"luaEventStruct.hpp"
#include"EventHandler.hpp"

EventStruct *EventStruct_new(lua_State *L)
{
    return new EventStruct();
}

int EventStruct_setEventType(lua_State *L)
{
    EventStruct *es = luaW_check<EventStruct>(L,1);
    es->setEventType((EVENT_TYPE)luaL_checknumber(L,2));
    return 0;
}

int EventStruct_setIntStruct(lua_State *L)
{
    EventStruct *es = luaW_check<EventStruct>(L,1);
    es->setIntStruct(luaL_checknumber(L,2));
    return 0;
}

int EventStruct_setVector2Struct(lua_State *L)
{
    EventStruct *es = luaW_check<EventStruct>(L,1);
    Vector2 vec;
    vec.x = luaL_checknumber(L,2);
    vec.y = luaL_checknumber(L,3);
    es->setVector2Struct(vec);
    return 0;
}

int EventStruct_setObjVecStruct(lua_State *L)
{
    EventStruct *es = luaW_check<EventStruct>(L,1);
    Vector2 vec;
    vec.x = luaL_checknumber(L,3);
    vec.y = luaL_checknumber(L,4);
    es->setObjVecStruct(luaW_check<GameObject>(L,2),vec);
    return 0;
}

int EventStruct_setObjIntStruct(lua_State *L)
{
    EventStruct *es = luaW_check<EventStruct>(L,1);
    es->setObjIntStruct(luaW_check<GameObject>(L,2),luaL_checknumber(L,3));
    return 0;
}

int EventStruct_setMapIntStruct(lua_State *L)
{
    EventStruct *es = luaW_check<EventStruct>(L,1);
    es->setMapIntStruct(luaW_check<MapState>(L,2),luaL_checknumber(L,3));
    return 0;
}

int EventStruct_setObjStruct(lua_State *L)
{
    EventStruct *es = luaW_check<EventStruct>(L,1);
    es->setObjStruct(luaW_check<GameObject>(L,2));
    return 0;
}

int EventStruct_setGameObjVecStruct(lua_State *L)
{
    EventStruct *es = luaW_check<EventStruct>(L,1);
    es->setGameObjVecStruct(luaW_check<GameObject>(L,2),luaW_check<GameObject>(L,3));
    return 0;
}

int EventStruct_setMoveFilter(lua_State *L)
{
    EventStruct *es = luaW_check<EventStruct>(L,1);
    es->setMoveFilter((MOVE_FILTER)luaL_checknumber(L,2));
    return 0;
}

int lua_sendEvent(lua_State *L)
{
    EventStruct *es = luaW_check<EventStruct>(L,1);
    EventHandler::sendEvent(*es);
    return 0;
}

int luaopen_EventStruct(lua_State *L)
{
    luaW_register<EventStruct>(L, "EventStruct", EventStruct_table, EventStruct_metatable, EventStruct_new);
    return 1;
}
