#ifndef LUAEVENTSTRUCT
#define LUAEVENTSTRUCT

#include"luawrapper.hpp"
#include"Event.hpp"

EventStruct *EventStruct_new(lua_State *L);

int EventStruct_setEventType(lua_State *L);
int EventStruct_setIntStruct(lua_State *L);
int EventStruct_setVector2Struct(lua_State *L);
int EventStruct_setObjVecStruct(lua_State *L);
int EventStruct_setObjIntStruct(lua_State *L);
int EventStruct_setMapIntStruct(lua_State *L);
int EventStruct_setObjStruct(lua_State *L);
int EventStruct_setGameObjVecStruct(lua_State *L);
int EventStruct_setMoveFilter(lua_State *L);

int lua_sendEvent(lua_State *L);

int luaopen_EventStruct(lua_State *L);

static luaL_Reg EventStruct_table[] = {
    {NULL,NULL}
};

static luaL_Reg EventStruct_metatable[] = {
    {"setEventType",EventStruct_setEventType},
    {"setIntStruct",EventStruct_setIntStruct},
    {"setVector2Struct",EventStruct_setVector2Struct},
    {"setObjVecStruct",EventStruct_setObjVecStruct},
    {"setObjIntStruct",EventStruct_setObjIntStruct},
    {"setMapIntStruct",EventStruct_setMapIntStruct},
    {"setObjStruct",EventStruct_setObjStruct},
    {"setGameObjVecStruct",EventStruct_setGameObjVecStruct},
    {"setMoveFilter",EventStruct_setMoveFilter},
    {NULL,NULL}
};

#endif
