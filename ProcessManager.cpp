#include"ProcessManager.hpp"

/*Clear the state stack.*/
ProcessManager::~ProcessManager()
{
    while(!stateStack.empty())
    {
        delete stateStack.top();
        stateStack.pop();
    }
}

/*Run current state*/
void ProcessManager::runState()
{
    while(!stateStack.empty())
    {
        stateStack.top()->run();
    }
}

/*Make new state*/
void ProcessManager::pushState(State *s)
{
    stateStack.push(s);
}

/*Remove current state.*/
void ProcessManager::popState()
{
    delete stateStack.top();
    stateStack.pop();
}

ProcessManager &ProcessManager::ProcessMan()
{
    static ProcessManager PD;
    return PD;
}
