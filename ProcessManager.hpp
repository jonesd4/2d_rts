#ifndef PROCESSMANAGER
#define PROCESSMANAGER

#include<stack>

#include"State.hpp"

/*
*Process managing service for the program.
*/
class ProcessManager
{
    public:
        explicit ProcessManager(){}; //Empty constructor
        ~ProcessManager(); //Deconstructor.

        void runState(); //Run current state.
        void popState(); //Remove current state.
        void pushState(State *); //Set a new current state.

        static ProcessManager &ProcessMan();

    private:
        std::stack<State*> stateStack; //Stack of game states.
};

#endif
