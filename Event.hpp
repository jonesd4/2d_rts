#ifndef EVENT
#define EVENT

#include"GameStructs.hpp"

class GameObject;
class MapState;

enum EVENT_TYPE {CLOSE_APP,
           OBJ_MOVE,
           INPUT,
           NEW_DEST,
           DEST_REACH,
           TAKE_DAMAGE,
           DESTROYED,
           ATTACK,
           HARVESTED,
           HARVEST,
           SPAWN,
           SPAWNED};

/*New code*/
struct IntStruct
{
    int value;
};

struct Vector2Struct
{
    Vector2 vector;
};

struct ObjVecStruct
{
    GameObject *gobj;
    Vector2 vec;
};

struct ObjIntStruct
{
    GameObject *gobj;
    int value;
};

struct ObjStruct
{
    GameObject *gobj;
};

struct GameObjVecStruct
{
    GameObjVec gameobjvec;
};

struct MapIntStruct
{
    MapState *mapstate;
    int value;
};

union EventUnion
{
    IntStruct intstruct;
    Vector2Struct vecstruct;
    ObjVecStruct objvecstruct;
    ObjIntStruct objintstruct;
    ObjStruct objstruct;
    GameObjVecStruct gameobjvecstruct;
    MapIntStruct mapintstruct;
};

enum MOVE_FILTER {USER_MOVED,
            ATTACK_MOVED};

union FilterUnion
{
    MOVE_FILTER movefilter;
};

class EventStruct
{
    public:
        EventStruct(){};
        EventStruct(EVENT_TYPE type,EventUnion data,FilterUnion filter): eventType(type), eventUnion(data), filterUnion(filter){}

        EVENT_TYPE getEventType(){return eventType;}
        EventUnion getEventUnion(){return eventUnion;}
        FilterUnion getFilterUnion(){return filterUnion;}

        void setEventType(EVENT_TYPE type){eventType = type;}

        void setIntStruct(int val){eventUnion.intstruct.value = val;}
        void setVector2Struct(Vector2 vec){eventUnion.vecstruct.vector = vec;}
        void setObjVecStruct(GameObject *go,Vector2 vec){eventUnion.objvecstruct.vec = vec; eventUnion.objintstruct.gobj = go;}
        void setObjIntStruct(GameObject *go, int val){eventUnion.objintstruct.value = val; eventUnion.objintstruct.gobj = go;}
        void setMapIntStruct(MapState *state, int val){eventUnion.mapintstruct.mapstate = state; eventUnion.mapintstruct.value = val;}
        void setObjStruct(GameObject *go){eventUnion.objstruct.gobj = go;}
        void setGameObjVecStruct(GameObject *go_one,GameObject *go_two){eventUnion.gameobjvecstruct.gameobjvec.gobjOne = go_one;eventUnion.gameobjvecstruct.gameobjvec.gobjTwo = go_two; }

        void setMoveFilter(MOVE_FILTER mf){filterUnion.movefilter = mf;}

    private:
        EVENT_TYPE eventType;
        EventUnion eventUnion;
        FilterUnion filterUnion;
};

#endif
