#include"Tile.hpp"
#include"IComponent.hpp"

GameObject::~GameObject()
{
    for(std::list<IComponent*>::iterator it = comList.begin(); it != comList.end(); it++)
    {
        delete (*it);
    }

    comList.clear();
}

void GameObject::addCom(IComponent *ic)
{
    comList.push_back(ic);
}

void GameObject::onEvent(const EVENT_TYPE &etype, const EventUnion &eunion, const FilterUnion &funion)
{
    for(std::list<IComponent*>::iterator it = comList.begin(); it != comList.end(); it++)
    {
        (*it)->onEvent(this,etype, eunion, funion);
    }
}

void GameObject::onUpdate()
{
    for(std::list<IComponent*>::iterator it = comList.begin(); it != comList.end(); it++)
    {
        (*it)->update(this);
    }
}
