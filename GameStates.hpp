#ifndef GAMESTATES
#define GAMESTATES

#include<map>
#include<list>
#include<iostream>

#include"lua.hpp"

#include"State.hpp"
#include"TileMap.hpp"
#include"EventListener.hpp"
#include"GameStructs.hpp"
#include"BoolMap.hpp"
#include"animation.hpp"
#include"utilities.hpp"
#include"GameCom.hpp"
#include"UIobj.hpp"

enum SHEET{MAP,OBJECT,UI};
enum OBJ_TYPE{SOLID,BOX,UNIT,RESOURCE,BUILDING};
enum FACTION {P1,P2,P1_ALLY,P2_ALLY,NETURAL,NONE};


class MapStateComm;
class tinyxml2::XMLElement;

static StringIntPair ObjTypeTable[] = {
    {"SOLID",SOLID},
    {"BOX",BOX},
    {"UNIT",UNIT},
    {"RESOURCE",RESOURCE},
    {"BUILDING",BUILDING},
    {"NULL",NULL}
};

static StringIntPair AniTypeTable[] = {
    {"HUMAN_IDLE",HUMAN_IDLE},
    {"HUMAN_WALK",HUMAN_WALK},
    {"HUMAN_CAST",HUMAN_CAST},
    {"NULL",NULL}
};

static StringIntPair FacTable[] = {
    {"P1",P1},
    {"P2",P2},
    {"P1_ALLY",P1_ALLY},
    {"P2_ALLY",P2_ALLY},
    {"NETURAL",NETURAL},
    {"NONE",NONE},
    {"NULL",NULL}
};

/*
*Map state of the game. will be used as the
*main state in the game.
*/

class PlayerController
{
    public:
        PlayerController(): selected(NULL), faction(NONE){}

        int &getFaction(){return faction;}
        int &getRecs(){ return recs; }
        GameObject *getSelected(){return selected;}

        void setRecs(int r){recs = r;}
        void setFaction(int fac){faction = fac;}
        void setSelected(GameObject *go){selected = go;}

        void update(MapState *);

    private:
        GameObject *selected;
        int faction;
        int recs;
        bool mousehold;
};

class MapState : public State, public EventListener
{
    public:
        explicit MapState(char *); //Create MapState using data file of the map.
        explicit MapState(){}

        void registerTypeMap(StringIntPair t[]);
        void registerComMap(StringIntPair t[]);
        void registerAniMap(StringIntPair t[]);
        void registerFacMap(StringIntPair t[]);

        /*TODO: could be factory method for objects*/
        void makeObj(tinyxml2::XMLElement *);
        void makeObj(int,int,int,int,int,const char *);
        void makeObj(int,int,int,int,int,OBJ_TYPE);
        void moveObj(GameObject *, int);
        void setObj(GameObject *);
        GameObject *makeEmptyObj();

        void onLoad();
        void onLoop();
        void onClose();
        void onEvent(const EVENT_TYPE &, const EventUnion &, const FilterUnion &);

        void updateComm();
        void updateGameObj();
        void updateUIobj();

        void drawUI();

        MapStateComm *addComm(MapStateComm *);

        int getX() const {return x;}
        int getY() const {return y;}
        int getMapWidth() const {return mapWidth;}
        int getMapHeight() const {return mapHeight;}
        lua_State *getLuaState() {return L;}
        BoolMap &getBoolMap(){return boolMap;}
        BoolMap *getBoolMapAdd(){return &boolMap;}
        TileMap &getTileMap(){return tileMap;}
        //AnimationSet &getAniMap(std::string str){return aniSetMap[str];}

        //GameObject *player;
        //GameObject *selected;
        //bool mousehold;

        PlayerController player;

    private:
        TileMap tileMap; //TileMap for game objects.
        BoolMap boolMap; //BoolMap for moving around the map.

        int x;
        int y;
        int cursorX;
        int cursorY;
        int mapWidth;
        int mapHeight;
        int recs;

        lua_State *L;

        std::map<std::string, int> typeIntMap;
        std::map<std::string, int> comIntMap;
        std::map<std::string, int> aniIntMap;
        std::map<std::string, int> facIntMap;
        std::map<std::string, AnimationSet> aniSetMap;

        std::list<GameObject> gameObjList;
        std::list<MapStateComm*> commandList;
        std::list<UIobj> uiList;
};

#endif
