#ifndef GAMECOM
#define GAMECOM

#include"IComponent.hpp"
#include"Tile.hpp"
#include"animation.hpp"
#include"utilities.hpp"

class MapStateComm;

enum DIRECTION{LEFT,
                RIGHT,
                UP,
                DOWN,
                LEFT_UP,
                RIGHT_UP,
                LEFT_DOWN,
                RIGHT_DOWN};

enum COM{MOVECOM,
        ATTACKABLECOM,
        ATTACKERCOM,
        RESOURCECOM,
        BUILDERCOM,
        BUILDINGCOM,
        ANIMATIONCOM};

static StringIntPair ComTable[] = {
    { "MOVECOM", MOVECOM},
    { "ATTACKABLECOM", ATTACKABLECOM},
    { "ATTACKERCOM", ATTACKERCOM},
    { "RESOURCECOM", RESOURCECOM},
    { "BUILDERCOM", BUILDERCOM},
    { "BUILDINGCOM", BUILDINGCOM},
    { "ANIMATIONCOM", ANIMATIONCOM},
    { "NULL", NULL}
};

/*
*class to move an object in a sliding fashion.
*/
class moveCom : public IComponent
{
    public:
        explicit moveCom(): offsetX(0), offsetY(0), moving(false), direction(0), objmovecomm(NULL){}
        static moveCom *newCom(){return new moveCom();}

        void onEvent(GameObject*,const EVENT_TYPE &, const EventUnion &, const FilterUnion &);
        void update(GameObject *);

    private:
        int offsetX;
        int offsetY;
        int direction;
        MapStateComm *objmovecomm;
        bool moving;
};

class attackableCom : public IComponent
{
    public:
        explicit attackableCom(int heal): health(heal){}
        static attackableCom *newCom(int health){return new attackableCom(health);}

        void onEvent(GameObject *,const EVENT_TYPE &, const EventUnion &, const FilterUnion &);
        void update(GameObject *);

    private:
        int health;
};

class attackerCom : public IComponent
{
    public:
        explicit attackerCom(): target(NULL), targetX(0), targetY(0), ready(false){}
        static attackerCom *newCom(){return new attackerCom();}

        void onEvent(GameObject *,const EVENT_TYPE &, const EventUnion &, const FilterUnion &);
        void update(GameObject *);

    private:
        bool ready;
        GameObject *target;
        int targetX;
        int targetY;
};

class resourceCom : public IComponent
{
    public:
        explicit resourceCom(): health(100){}
        static resourceCom *newCom(){return new resourceCom();}

        void onEvent(GameObject *,const EVENT_TYPE &, const EventUnion &, const FilterUnion &);
        void update(GameObject *);

    private:
        int health;
};

class builderCom : public IComponent
{
    public:
        explicit builderCom(): target(NULL), ready(false){}
        static builderCom *newCom(){return new builderCom();}

        void onEvent(GameObject *, const EVENT_TYPE &, const EventUnion &, const FilterUnion &);
        void update(GameObject *);

    private:
        GameObject *target;
        bool ready;
};

class buildingCom : public IComponent
{
    public:
        explicit buildingCom() : wait(false){}
        static buildingCom *newCom(){return new buildingCom();}

        void onEvent(GameObject *, const EVENT_TYPE &, const EventUnion &, const FilterUnion &);
        void update(GameObject *);

    private:
        bool wait;
};

class animationCom : public IComponent
{
    public:
        explicit animationCom(AnimationSet& aSet) : state(HUMAN_IDLE), aniSet(aSet), delay(20), count(0){};
        static animationCom *newCom(AnimationSet &aSet){return new animationCom(aSet);}

        void onEvent(GameObject *, const EVENT_TYPE &, const EventUnion &, const FilterUnion &);
        void update(GameObject *);

    private:
        int state;
        AnimationSet &aniSet;
        int delay;
        int count;
};
#endif
